$(document).ready(function() {
	$("#myTable").tablesorter(); 
	$("#myTable2").tablesorter(); 
	
	//zmena sort znaciek v headeri tabulky	
	$("#myTable th").click(
		function(){
			setTimeout(function(){
				$('#myTable th').each(
					function(){
						if ($(this).hasClass('headerSortDown')){
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort-by-attributes');
						}else if($(this).hasClass('headerSortUp')){
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort-by-attributes-alt');
						}else{
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort');
						}
					}
				);	
			}, 1);	
		}
	);
	
	$("#myTable2 th").click(
		function(){
			setTimeout(function(){
				$('#myTable2 th').each(
					function(){
						if ($(this).hasClass('headerSortDown')){
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort-by-attributes');
						}else if($(this).hasClass('headerSortUp')){
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort-by-attributes-alt');
						}else{
							$('span', this).removeClass();
							$('span', this).addClass('glyphicon glyphicon-sort');
						}
					}
				);	
			}, 1);	
		}
	);
	
	$("#profil_submit_meno").click(function(event) {
		event.preventDefault();
		var meno = $("input#meno").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_meno_submit",
		
			data: {'meno': meno},
			success: function(result){
				if (result.charAt(0) != '<'){
					zmen_form_na_pociatocny_stav('meno', result);		
				}else{
					ukaz('meno_err');
					ukaz_validacne_chyby('meno', result);	
				}	
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#profil_submit_priezvisko").click(function(event) {
		event.preventDefault();
		var priezvisko = $("input#priezvisko").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_priezvisko_submit",
		
			data: {'priezvisko': priezvisko},
			success: function(result){
				if (result.charAt(0) != '<'){
					zmen_form_na_pociatocny_stav('priezvisko', result);		
				}else{
					ukaz('priezvisko_err');
					ukaz_validacne_chyby('priezvisko', result);	
				}	
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#profil_submit_email").click(function(event) {
		event.preventDefault();
		var email = $("input#email").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_email_submit",
		
			data: {'email': email},
			success: function(result){
				if (result.charAt(0) != '<'){
					zmen_form_na_pociatocny_stav('email', result);		
				}else{
					ukaz('email_err');
					ukaz_validacne_chyby('email', result);	
				}	
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	
	$("#profil_submit_datum").click(function(event) {
		event.preventDefault();
		var datum_nar = $("input#datum_nar").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_datumu_submit",
		
			data: {'datum_nar': datum_nar},
			success: function(result){
				if (result.charAt(0) != '<'){
					zmen_form_na_pociatocny_stav('datum_nar', result);		
				}else{
					ukaz('datum_nar_err');
					ukaz_validacne_chyby('datum_nar', result);	
				}
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#profil_submit_vyska").click(function(event) {
		event.preventDefault();
		var vyska = $("input#vyska").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_vysky_submit",
		
			data: {'vyska': vyska},
			success: function(result){
				if (result.charAt(0) != '<'){
					if (result == "0 cm"){
						zmen_form_na_pociatocny_stav('vyska', "- cm");
					}else{
						zmen_form_na_pociatocny_stav('vyska', result);
					}						
				}else{
					ukaz('vyska_err');
					ukaz_validacne_chyby('vyska', result);	
				}					
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#profil_submit_vaha").click(function(event) {
		event.preventDefault();
		var vaha = $("input#vaha").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_vahy_submit",
		
			data: {'vaha': vaha},
			success: function(result){
				if (result.charAt(0) != '<'){
					if (result == "0 kg"){
						zmen_form_na_pociatocny_stav('vaha', "- kg");
					}else{
						zmen_form_na_pociatocny_stav('vaha', result);
					}						
				}else{
					ukaz('vaha_err');
					ukaz_validacne_chyby('vaha', result);	
				}
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#profil_submit_heslo").click(function(event) {
		event.preventDefault();
		var heslo = $("input#pass1").val();
		var heslo2 = $("input#pass2").val();
		jQuery.ajax({
			type: "POST",
			url: base_url+"profil/zmena_hesla",
		
			data: {'pass1': heslo, 'pass2' : heslo2},
			success: function(result){
				if (result == true){
					ukaz_validacne_chyby('zmena_hesla', '<div id="flash-messages" class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a>Heslo bolo zmenené.</div>');
					schovaj('zmena_hesla_form');
					ukaz('zmena_hesla_button');
				}else{
					ukaz_validacne_chyby('zmena_hesla', result);
				}
			},
			error: function(xhr, error){
				console.debug(xhr); console.debug(error);
			}
		});
	});
	
	$("#flash-messages").click(function(){$(this).fadeOut(500)});
});

function zmen_form_na_pociatocny_stav(elem_id, datum){
	$("#"+elem_id+"_err").addClass("none");
	$("#"+elem_id+"_err").removeClass("inline-block");
	
	$("#"+elem_id+"_form").addClass("none");
	$("#"+elem_id+"_form").removeClass("inline-block");
	
	$("#"+elem_id+"_text").addClass("inline-block");
	$("#"+elem_id+"_text").removeClass("none");
	
	$("#"+elem_id+"_edit").addClass("inline-block");
	$("#"+elem_id+"_edit").removeClass("none");
	
	$("#"+elem_id+"_text").html(datum);
}

function ukaz_validacne_chyby(elem_id, errors){
	$("#"+elem_id+"_err").html(errors);
}

function ukaz_form_pre_zalozenie_timu(){
	$("#form_pre_zalozenie_timu").removeClass("none");
	$("#form_pre_zalozenie_timu").addClass("block");
	
	$("#button_zaloz_tim").removeClass("inline-block");
	$("#button_zaloz_tim").addClass("none");
}

function skry_form_pre_zalozenie_timu(){
	$("#form_pre_zalozenie_timu").removeClass("display-block");	
	$("#form_pre_zalozenie_timu").addClass("none");
	
	$("#button_zaloz_tim").removeClass("none");
	$("#button_zaloz_tim").addClass("inline-block");	
}

function zobraz_textareu_popis_timu(){
	$("#textarea_popisu_timy").removeClass("none");	
	$("#textarea_popisu_timy").addClass("display-block");
	
	$("#text_popisu_timy").removeClass("display-block");	
	$("#text_popisu_timy").addClass("none");
}

function schovaj_textareu_popis_timu(){
	$("#textarea_popisu_timy").removeClass("display-block");	
	$("#textarea_popisu_timy").addClass("none");
	
	$("#text_popisu_timy").removeClass("none");	
	$("#text_popisu_timy").addClass("display-block");
}

function ukaz_form_potvrdenie_zrusenia_timu(){
	$("#form_zrusenie_timu").removeClass("none");	
	$("#form_zrusenie_timu").addClass("display-block");
	
	$("#cudlik_zrusenie_timu").removeClass("display-block");	
	$("#cudlik_zrusenie_timu").addClass("none");
}

function schovaj_form_potvrdenie_zrusenia_timu(){
	$("#form_zrusenie_timu").removeClass("display-block");	
	$("#form_zrusenie_timu").addClass("none");
	
	$("#cudlik_zrusenie_timu").removeClass("none");	
	$("#cudlik_zrusenie_timu").addClass("display-block");
}

function ukaz(elem_id){
	$("#"+elem_id).removeClass("none");	
	$("#"+elem_id).addClass("display-block");
}

function schovaj(elem_id){
	$("#"+elem_id).removeClass("display-block");	
	$("#"+elem_id).addClass("none");
}

function vymaz_obsah(elem_id){
	$("#"+elem_id).empty();
}

function vymaz_val(elem_id){
	$("#"+elem_id).val('');
}

function nastav_val(elem_id, val){
	$("#"+elem_id).val(val);
}

