-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Út 15.Mar 2016, 20:45
-- Verzia serveru: 5.5.24-log
-- Verzia PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `tia`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `clenovia`
--

CREATE TABLE IF NOT EXISTS `clenovia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_timu` int(11) NOT NULL,
  `id_usera` int(11) NOT NULL,
  `potvrdeny` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Sťahujem dáta pre tabuľku `clenovia`
--

INSERT INTO `clenovia` (`id`, `id_timu`, `id_usera`, `potvrdeny`) VALUES
(1, 1, 3, 1),
(2, 4, 3, 1),
(4, 5, 3, 1),
(13, 1, 2, 1),
(18, 5, 2, 1),
(19, 10, 2, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `timy`
--

CREATE TABLE IF NOT EXISTS `timy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_admina` int(11) NOT NULL,
  `nazov` varchar(50) NOT NULL,
  `popis` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Sťahujem dáta pre tabuľku `timy`
--

INSERT INTO `timy` (`id`, `id_admina`, `nazov`, `popis`) VALUES
(1, 3, 'Miesici', 'Sme topka!\r\nNajlepsi tim ever!!!!'),
(4, 3, 'Supaci', 'popis'),
(5, 3, 'Croky', 'Sme proste Corky\r\nNikto to nezmeni\r\nViniciarske corky\r\nVsetko vyhrame a ked nie, tak ukradeneme pohar'),
(10, 2, 'Supa team', '### ###\r\n_#_ ##_\r\n_#_ ###');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `ucastnici`
--

CREATE TABLE IF NOT EXISTS `ucastnici` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_zapasu` int(11) NOT NULL,
  `id_usera` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Sťahujem dáta pre tabuľku `ucastnici`
--

INSERT INTO `ucastnici` (`id`, `id_zapasu`, `id_usera`) VALUES
(11, 5, 3);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `udalosti`
--

CREATE TABLE IF NOT EXISTS `udalosti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazov` varchar(50) NOT NULL,
  `popis` text NOT NULL,
  `miesto` varchar(50) NOT NULL,
  `datum` date NOT NULL,
  `cas` time NOT NULL,
  `id_admina` int(11) NOT NULL,
  `typ` int(11) NOT NULL,
  `verejne` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Sťahujem dáta pre tabuľku `udalosti`
--

INSERT INTO `udalosti` (`id`, `nazov`, `popis`, `miesto`, `datum`, `cas`, `id_admina`, `typ`, `verejne`) VALUES
(5, 'Vinicne vs. Grob', 'Pridte povzbudit nasich\r\nBude PIVOOOO\r\nAj kofola', 'Ihrisko Vinicne', '2016-03-25', '10:03:00', 3, 2, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meno` varchar(20) NOT NULL,
  `priezvisko` varchar(20) NOT NULL,
  `heslo` varchar(128) NOT NULL,
  `email` varchar(60) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `foto` varchar(50) NOT NULL DEFAULT 'default.jpg',
  `datum_nar` date NOT NULL,
  `vyska` float NOT NULL,
  `vaha` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Sťahujem dáta pre tabuľku `users`
--

INSERT INTO `users` (`id`, `meno`, `priezvisko`, `heslo`, `email`, `admin`, `foto`, `datum_nar`, `vyska`, `vaha`) VALUES
(2, 'Michal', 'Sejc', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'kundrikova@centrum.sk', 0, 'default.jpg', '0000-00-00', 50, 12),
(3, 'Sejco', 'Sejc', '011c945f30ce2cbafc452f39840f025693339c42', 'm1s00@centrum.cz', 0, 'b5eae5500cf48e5330d7402c36c7637c.jpg', '2000-05-15', 180, 65),
(4, 'juro', 'juro', '011c945f30ce2cbafc452f39840f025693339c42', 'zzz@zzz.zzz', 0, 'default.jpg', '0000-00-00', 0, 0),
(5, 'aaa', 'daa', '011c945f30ce2cbafc452f39840f025693339c42', 'aaa@aaa.aa', 0, 'default.jpg', '0000-00-00', 0, 0),
(6, 'Jozef', 'Frn', '011c945f30ce2cbafc452f39840f025693339c42', 'dd@dd.dd', 0, 'default.jpg', '0000-00-00', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
