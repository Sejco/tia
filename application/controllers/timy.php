<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Timy extends CI_Controller{
		
		function __construct(){
			parent::__construct(); 	
			
			$this->load->model('timy_model');	
			$this->load->model('udalosti_model');	

			$this->load->helper('typography');	
			
			$this->data['title'] 		= 'Teamer - Tímy';
			$this->data['je_admin']	= false;
			if ($this->session->userdata('admin') == 1){
				$this->data['je_admin'] = true;
			}
		}
		
		function _pridaj_info_o_pocte_clenov(){
			if (count($this->data['timy']) > 0){
				for ($i = 0; $i < count($this->data['timy']); $i++){
					$this->data['timy'][$i]['pocet_clenov'] = $this->timy_model->nacitaj_pocet_clenov_timu($this->data['timy'][$i]['id']);
				}
			}
		}
		
		function _pridaj_info_o_pocte_ucastnikov(){
			if (count($this->data['udalosti']) > 0){
				for ($i = 0; $i < count($this->data['udalosti']); $i++){
					$this->data['udalosti'][$i]['pocet_ucastnikov'] = $this->udalosti_model->nacitaj_pocet_ucastnikov_udalosti($this->data['udalosti'][$i]['id']);
				}
			}
		}
		
		function _pridaj_moznosti_pre_tim(){
			$this->data['je_clen'] 		= false;
			$this->data['ma_prihlasku'] = false;
					
			if ($this->timy_model->je_clenom_timu_user($this->session->userdata('id'), $this->uri->segment(2))){
				$this->data['je_clen'] 	= true;
			}			
			if ($this->timy_model->ma_podanu_prihlasku_user($this->session->userdata('id'), $this->uri->segment(2))){
				$this->data['ma_prihlasku'] = true;
			}		
		}
		
		function index(){
			$this->data['timy'] = $this->timy_model->nacitaj_vsetky_timy();			
			$this->_pridaj_info_o_pocte_clenov();			
			$this->load->view('timy_view', $this->data);
		}
		
		function hladat_tim(){
			$this->data['timy'] = $this->timy_model->nacitaj_hladane_timy($this->security->xss_clean($this->input->get('timy_search')));			
			$this->_pridaj_info_o_pocte_clenov();							
			$this->load->view('timy_view', $this->data);
		}	
		
		function ukaz_detail_timu($id_timu){
			if ($this->timy_model->existuje_tim($id_timu)){
				$this->data['detail_timu'] 		= $this->timy_model->nacitaj_tim_podla_id($id_timu);
				$this->data['clenovia'] 		= $this->timy_model->nacitaj_clenov_timu($id_timu);
				$this->data['je_admin_timu'] 	= $this->data['detail_timu']['id_admina'] == $this->session->userdata('id');
				$this->data['pocet_clenov'] 	= $this->timy_model->nacitaj_pocet_clenov_timu($id_timu);
				$this->data['udalosti']			= $this->timy_model->nacitaj_udalosti_timu($id_timu);
				$this->_pridaj_info_o_pocte_ucastnikov();
				
				if ($this->session->userdata('prihlaseny')){
					$this->_pridaj_moznosti_pre_tim();				
				}				
				$this->load->view('detail_timu_view', $this->data);
			}else{
				$this->data['error_text'] = 'Tím neexistuje.';				
				$this->load->view('error_view', $this->data);	
			}		
		}
		
		function schval_clena($id_timu, $id_usera){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if ($this->timy_model->je_admin_timu($this->session->userdata('id'), $id_timu)){
						$this->timy_model->schval_clena_timu($id_timu, $id_usera);						
						$this->session->set_flashdata('message', 'Žiadosť užívateľa bola schválená!');						
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
		function vyhod_clena_z_timu($id_timu, $id_usera){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if ($this->timy_model->je_admin_timu($this->session->userdata('id'), $id_timu)){
						if ($this->session->userdata('id') != $id_usera){
							$this->timy_model->vyhod_clena_timu($id_timu, $id_usera);						
							$this->session->set_flashdata('message', 'Užívateľ bol z tímu odstránený!');	
							$pom_udaloti_timu = $this->timy_model->nacitaj_udalosti_timu($id_timu);
							for ($ii = 0; $ii < count($pom_udaloti_timu); $ii++){
								$this->udalosti_model->odober_ucastnika($pom_udaloti_timu[$ii]['id'], $id_usera);			
							}
						}
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
		function zmen_popis($id_timu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if ($this->timy_model->je_admin_timu($this->session->userdata('id'), $id_timu)){
						$this->load->library('form_validation');
						$this->form_validation->set_rules('popis_timu', 'Popis', 'trim|htmlspecialchars');						
						if ($this->form_validation->run()){
							$this->timy_model->zmen_popis_timu($id_timu, $_POST['popis_timu']);
							$this->session->set_flashdata('message', 'Popis bol zmenený!');										
						}
					}	
				}
			}	
			redirect('timy/'.$id_timu);
		}
		
		function podat_prihlasku($id_timu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if (!$this->timy_model->je_clenom_timu_user($this->session->userdata('id'), $this->uri->segment(2))){
						if (!$this->timy_model->ma_podanu_prihlasku_user($this->session->userdata('id'), $this->uri->segment(2))){
							$this->timy_model->pridaj_clena_timu($id_timu, $this->session->userdata('id'), 0);							
							$this->session->set_flashdata('message', 'Priháška do tímu bola úspešne odoslaná!');					
						}
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
		function zrusit_prihlasku($id_timu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if ($this->timy_model->ma_podanu_prihlasku_user($this->session->userdata('id'), $this->uri->segment(2))){
						$this->timy_model->vyhod_clena_timu($id_timu, $this->session->userdata('id'));					
						$this->session->set_flashdata('message', 'Priháška bola stiahnutá!');													
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
		function opustit_tim($id_timu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if ($this->timy_model->je_clenom_timu_user($this->session->userdata('id'), $this->uri->segment(2))){
						$this->timy_model->vyhod_clena_timu($id_timu, $this->session->userdata('id'));						
						$this->session->set_flashdata('message', 'Opustil si tím!');
						$pom_udaloti_timu = $this->timy_model->nacitaj_udalosti_timu($id_timu);
						for ($ii = 0; $ii < count($pom_udaloti_timu); $ii++){
							$this->udalosti_model->odober_ucastnika($pom_udaloti_timu[$ii]['id'], $this->session->userdata('id'));			
						}
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
		function zrusit_tim($id_timu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->timy_model->existuje_tim($id_timu)){
					if (($this->timy_model->je_admin_timu($this->session->userdata('id'), $this->uri->segment(2))) || ($this->session->userdata('admin') == 1)){				
						if ($this->timy_model->zadal_spravne_heslo($this->session->userdata('id'), $_POST['potvrdzovacie_heslo'])){
							$this->timy_model->vyhod_vsetkych_clenov_timu($id_timu);
							$this->timy_model->zmaz_tim($id_timu);							
							$this->session->set_flashdata('message', 'Tím bol rozpustený!');									
							redirect('profil/'.$this->session->userdata('id'));
						}else{							
							$this->session->set_flashdata('error', 'Nesprávne heslo!');
							redirect('timy/'.$id_timu);
						}
					}
				}
			}
			redirect('timy/'.$id_timu);
		}
		
	}
?>