<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Udalosti extends CI_Controller{
		
		function __construct(){
			parent::__construct(); 	
			
			$this->load->model('udalosti_model');	
			$this->load->model('timy_model');	

			$this->load->helper('typography');	
			
			$this->data['je_admin_udalosti'] = false;
			$this->data['je_ucastnikom'] 	 = false;
			$this->data['moj_stav']			 = 0;
			$this->data['title']			 = 'Teamer - Udalosti';
			$this->data['je_adminom']		 = false;
			if ($this->session->userdata('admin') == 1){
				$this->data['je_adminom'] = true;
			}
		}
		
		function _pridaj_info_o_pocte_ucastnikov(){
			if (count($this->data['udalosti']) > 0){
				for ($i = 0; $i < count($this->data['udalosti']); $i++){
					$this->data['udalosti'][$i]['pocet_ucastnikov'] = $this->udalosti_model->nacitaj_pocet_ucastnikov_udalosti($this->data['udalosti'][$i]['id']);
				}
			}
		}
		
		function _pridel_pravomoci(){
			if ($this->data['udalost']['id_admina'] == $this->session->userdata('id')){
				$this->data['je_admin_udalosti'] = true;
			}
			if ($this->udalosti_model->je_ucatnikom_udalosti($this->data['udalost']['id'], $this->session->userdata('id'))){
				$this->data['je_ucastnikom'] 	 = true;
			}
			$this->data['moj_stav'] = $this->udalosti_model->nacitaj_moj_stav($this->data['udalost']['id'], $this->session->userdata('id'));
		}
		
		function index(){
			$this->data['udalosti'] = $this->udalosti_model->nacitaj_vsetky_udalosti();
			
			$this->_pridaj_info_o_pocte_ucastnikov();
			
			$this->load->view('udalosti_view', $this->data);
		}
		
		function hladat_udalost(){
			$this->data['udalosti'] = $this->udalosti_model->nacitaj_hladane_udalosti($this->security->xss_clean($this->input->get('udalosti_search')));
			
			$this->_pridaj_info_o_pocte_ucastnikov();		
					
			$this->load->view('udalosti_view', $this->data);
		}
		
		function ukaz_detail_udalosti($id_udalosti){
			if ($this->udalosti_model->existuje_udalost($id_udalosti)){
				$this->data['udalost'] 			= $this->udalosti_model->nacitaj_udalost_podla_id($id_udalosti);
				$this->data['pocet_ucastnikov'] = $this->udalosti_model->nacitaj_pocet_ucastnikov_udalosti($id_udalosti);
				$this->data['ucastnici'] 		= $this->udalosti_model->nacitaj_ucastnikov_udalosti($id_udalosti);
				$this->data['komentare'] 		= $this->udalosti_model->nacitaj_komentare_udalosti($id_udalosti); 
				
				if ($this->data['udalost']['verejne'] == "0"){
					// je verejna				
					$this->_pridel_pravomoci();					
					$this->load->view('detail_udalosti_view', $this->data);
				}else{
					if ($this->udalosti_model->som_v_time_ktory_organizuje_udalost($this->session->userdata('id'), $this->data['udalost']['verejne'])){	
						$this->data['udalost']['nazov_timu'] = $this->timy_model->nacitaj_nazov_timu($this->data['udalost']['verejne']);						
						$this->_pridel_pravomoci();									
						$this->load->view('detail_udalosti_view', $this->data);
					}else{
						$this->data['error_text'] = 'Táto udalosť je súkromná';				
						$this->load->view('error_view', $this->data);		
					}	
				}
			}else{
				$this->data['error_text'] = 'Udalosť neexistuje.';				
				$this->load->view('error_view', $this->data);
			}
			
		}
		
		function pridaj_usera_k_udalosti($id_udalosti, $stav){
			if ($this->session->userdata('prihlaseny')){
				if (($stav == 1) || ($stav == 2) || ($stav == 3)){
					if ($this->udalosti_model->existuje_udalost($id_udalosti)){
						$this->data['udalost'] = $this->udalosti_model->nacitaj_udalost_podla_id($id_udalosti);				
						if (!$this->udalosti_model->je_ucatnikom_udalosti($this->data['udalost']['id'], $this->session->userdata('id'))){							
							if ($this->data['udalost']['verejne'] == "0"){						
								$this->udalosti_model->pridaj_ucastnika($id_udalosti, $this->session->userdata('id'), $stav);
								$this->session->set_flashdata('message', 'Bol si prihlásený na udalosť!');		
							}else{
								if ($this->udalosti_model->som_v_time_ktory_organizuje_udalost($this->session->userdata('id'), $this->data['udalost']['verejne'])){							
									$this->udalosti_model->pridaj_ucastnika($id_udalosti, $this->session->userdata('id'), $stav);							
									$this->session->set_flashdata('message', 'Bol si prihlásený na udalosť!');	
								}
							}
						}else{
							if ($this->udalosti_model->nacitaj_moj_stav($id_udalosti, $this->session->userdata('id')) != $stav){
								if ($this->data['udalost']['verejne'] == "0"){						
									$this->udalosti_model->zmen_stav_ucastnika($id_udalosti, $this->session->userdata('id'), $stav);
									$this->session->set_flashdata('message', 'Stav tvojej účasti bol zmenený!');		
								}else{
									if ($this->udalosti_model->som_v_time_ktory_organizuje_udalost($this->session->userdata('id'), $this->data['udalost']['verejne'])){
										$this->udalosti_model->zmen_stav_ucastnika($id_udalosti, $this->session->userdata('id'), $stav);							
										$this->session->set_flashdata('message', 'Stav tvojej účasti bol zmenený!');	
									}
								}	
							}
						}
					}
				}
			}			
			redirect('udalosti/'.$id_udalosti);
		}
		
		function pridaj_koment($id_udalosti){
			if ($this->session->userdata('prihlaseny')){
				if ($this->udalosti_model->existuje_udalost($id_udalosti)){
					$this->data['udalost'] = $this->udalosti_model->nacitaj_udalost_podla_id($id_udalosti);
					$this->load->library('form_validation');
					$this->form_validation->set_rules('text_komentu', 'Komentar', 'trim|htmlspecialchars|required');				
					if ($this->form_validation->run()){
						if ($this->data['udalost']['verejne'] == "0"){						
							$this->udalosti_model->vloz_komentar($id_udalosti, $this->session->userdata('id'), $_POST['text_komentu']);
							$this->session->set_flashdata('message', 'Váš komentár bol pridaný!');		
						}else{
							if ($this->udalosti_model->som_v_time_ktory_organizuje_udalost($this->session->userdata('id'), $this->data['udalost']['verejne'])){
								$this->udalosti_model->vloz_komentar($id_udalosti, $this->session->userdata('id'), $_POST['text_komentu']);							
								$this->session->set_flashdata('message', 'Váš komentár bol pridaný!');	
							}
						}	
					}
				}
			}
			redirect('udalosti/'.$id_udalosti);
		}
		
		function zrus_ucast($id_udalosti){
			if ($this->session->userdata('prihlaseny')){
				if ($this->udalosti_model->existuje_udalost($id_udalosti)){
					$this->data['udalost'] = $this->udalosti_model->nacitaj_udalost_podla_id($id_udalosti);				
					if ($this->udalosti_model->je_ucatnikom_udalosti($this->data['udalost']['id'], $this->session->userdata('id'))){						
						$this->udalosti_model->odober_ucastnika($id_udalosti, $this->session->userdata('id'));							
						$this->session->set_flashdata('message', 'Bol si odhlásený z udalosti!');						
					}
				}
			}			
			redirect('udalosti/'.$id_udalosti);
		}
		
		function zrusit_udalost($id_udalosti){
			if ($this->session->userdata('prihlaseny')){
				if ($this->udalosti_model->existuje_udalost($id_udalosti)){
					if (($this->udalosti_model->je_adminom_udalosti($this->session->userdata('id'), $id_udalosti)) || ($this->session->userdata('admin') == 1)){
						if ($this->udalosti_model->zadal_spravne_heslo($this->session->userdata('id'), $_POST['potvrdzovacie_heslo'])){
							$this->udalosti_model->vyhod_ucastnikov_udalosti($id_udalosti);
							$this->udalosti_model->zmaz_udalost($id_udalosti);						
							$this->session->set_flashdata('message', 'Udalosť bola zrušená!');									
							redirect('profil/'.$this->session->userdata('id'));
						}else{							
							$this->session->set_flashdata('error', 'Nesprávne heslo!');
							redirect('udalosti/'.$id_udalosti);
						}
					}
				}
			}
		}
		
		function zmen_popis($id_udalosti){
			if ($this->session->userdata('prihlaseny')){
				if ($this->udalosti_model->existuje_udalost($id_udalosti)){
					if ($this->udalosti_model->je_adminom_udalosti($this->session->userdata('id'), $id_udalosti)){
						$this->load->library('form_validation');
						$this->form_validation->set_rules('popis_udalosti', 'Popis', 'trim|htmlspecialchars');					
						if ($this->form_validation->run()){
							$this->udalosti_model->zmen_popis_udalosti($id_udalosti, $_POST['popis_udalosti']);								
							$this->session->set_flashdata('message', 'Popis bol zmenený!');								
							redirect('udalosti/'.$id_udalosti);
						}
					}
				}				
			}
		}
		
		function del_koment($id_udalosti, $id_komentu){
			if ($this->session->userdata('prihlaseny')){
				if ($this->session->userdata('admin') == 1){			
					$this->udalosti_model->odstran_koment($id_komentu);
					$this->session->set_flashdata('message', 'Komentár odstránený!');						
					redirect('udalosti/'.$id_udalosti);
				}
			}
			redirect('udalosti/'.$id_udalosti);
		}
		
		
	}
?>