<?php 
	
	class Auth extends CI_Controller{
		
		function __construct(){
			parent::__construct(); 	
			$this->load->model('auth_model');
			
			$this->data['zobraz_err_div'] = 'display-none';
			$this->data['title'] = 'Teamer';
		}
		
		function index(){
			$data = $this->data;
			
			if ($this->session->userdata('prihlaseny')){
				redirect('/home/');
			}else{
				$this->load->view('register_view', $data);
			}
				
		}
				
		function _logni_usera(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Prihlasovacie meno', 'trim|required');
			$this->form_validation->set_rules('pass', 'Heslo', 'trim|required');
			$this->form_validation->set_error_delimiters('<p>', '</p>');

			if ($this->form_validation->run()){
				$userData = $this->auth_model->check(); 
				if ($userData != false){
					$userData['prihlaseny'] = true;			
					$this->session->set_userdata($userData);
					
					redirect('/home/');
				}else{
					$this->data['zobraz_err_div'] = 'display-block';
				}
			}else{
				$this->data['zobraz_err_div'] = 'display-block';
			}		
		}
		
		function login(){	
			if (isset($_POST['submit'])){
				$this->_logni_usera();
			}
			$this->data['title'] = 'Teamer - Prihlásenie';
			$this->load->view('login_view', $this->data);
		}
		
		function logout(){			
			$pole_na_unset = array('id', 'meno', 'priezvisko', 'email', 'prihlaseny', 'admin');
			
			$this->session->unset_userdata($pole_na_unset);
			$this->session->sess_destroy();
				
			redirect('/home/');
		}
		
		function _pridaj_usera(){			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('meno', 'Meno', 'trim|required|alpha|min_length[3]');
			$this->form_validation->set_rules('priezvisko', 'Priezvisko', 'trim|required|alpha|min_length[3]');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|is_unique[users.email]');
			$this->form_validation->set_rules('heslo', 'Heslo', 'trim|required|min_length[6]');
			$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>');
			
			if ($this->form_validation->run() && $this->auth_model->register()){
				$this->session->set_flashdata('message', 'Boli ste úspešne zaregistrovaný. Teraz sa môžete prihlásiť.');
				
				redirect('/login/');	
			}	
		}
		
		function registracia(){
			if (isset($_POST['submit'])){
				$this->_pridaj_usera();
			}
			if ($this->session->userdata('prihlaseny')){
				redirect('/home/');
			}else{
				$this->data['title'] = 'Teamer - Registrácia';	
				$this->load->view('register_view', $this->data);
			}
		}
		
		function password_recovery(){
			if (isset($_POST['submit'])){
				$this->load->library('form_validation');
				$this->form_validation->set_rules('email', 'E-mail', 'trim');
				
				if ($this->form_validation->run()){
					if ($this->auth_model->existuje_email($_POST['email'])){
						$nove_heslo = $this->_generuj_heslo(10);			
						$this->_posli_email_s_heslom($nove_heslo, $_POST['email']);
						$this->auth_model->zmen_heslo_podla_mailu($nove_heslo, $_POST['email']);
						$this->session->set_flashdata('message', 'Na zadaný email bolo odoslané nové heslo.');
						redirect('/login/'); 
					}else{
						$this->session->set_flashdata('error', 'Zadaný email neexistuje');
					}
				}
			}
			
			$this->load->view('password_view', $this->data);
		}
		
		function _generuj_heslo($length = 8){
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		function _posli_email_s_heslom($heslo, $mail){
			$this->load->library('email');

			$this->email->from('teamer@password.sk', 'Teamer');
			$this->email->to($mail); 

			$this->email->subject('Teamer - Nové heslo');
			$this->email->message('Vaše nové heslo je '.$heslo.'. Odporúčame Vám si ho ihneď po prihlásení zmeniť.');	

			$this->email->send();		
		}
	}
?>