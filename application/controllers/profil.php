<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Profil extends CI_Controller{
		
		function __construct(){
			parent::__construct(); 	
			$this->load->model('profil_model');
			$this->load->model('timy_model');
			$this->load->model('udalosti_model');
			
			$this->data['zobraz_form_vytvorenie_timu']	 = false;
			$this->data['zobraz_form_vytvorenie_zapasu'] = false;
			$this->data['title']			 			 = 'Teamer - Profil';
			
			if (($this->session->userdata('prihlaseny')) && ($this->uri->segment(2) == $this->session->userdata('id'))){ 
				$this->data['povolenie_vytvorit_tim'] = true;
			}else{
				$this->data['povolenie_vytvorit_tim'] = false;
			}
		}
		
		function _process_image($image_data){
			$config['image_library'] 	= 'gd2';
			$config['source_image']		= $image_data['full_path'];
			$config['maintain_ratio'] 	= TRUE;
			$config['width']			= 150;
			$config['height']			= 200;

			$this->load->library('image_lib', $config); 

			if ($this->image_lib->resize()){
				return true;
			}else{
				return false;
			}
		}
				
		function _upload($id_usera){
			$path = './images/'.$id_usera.'/';
			
			if (!is_dir($path)){
				mkdir($path, 0777);
			}
			
			$config['upload_path'] 		= $path;
			$config['allowed_types'] 	= 'gif|jpg|png';
			$config['encrypt_name'] 	= TRUE;
			
			$this->load->library('upload', $config);
			$this->load->helper("file");
			delete_files($path);
			
			if ($this->upload->do_upload('profil_foto')){
					
				$data_suboru = $this->upload->data();
				
				if ($this->_process_image($data_suboru)){			
					$this->profil_model->pridaj_obrazok_userovi($id_usera, $data_suboru['file_name']);
										
					
					$this->session->set_flashdata('message', 'Fotografia bola zmenená!');
				}else{
					$this->session->set_flashdata('error', 'Pri spracovaní fotografie došlo k chybe!');
				}	
					
				redirect('profil/'.$this->session->userdata('id'));	
			}else{
				$this->session->set_flashdata('error', 'Pri nahrávaní fotografie došlo k chybe. Povolené formáty sú iba .jpg .png a .gif!');
					
				redirect('profil/'.$this->session->userdata('id'));
			}
		}
		
		function zobraz_profil_usera($id_usera){
			$this->data['user_info'] = $this->profil_model->nacitaj_data_usera($id_usera);
			$this->data['timy'] 	 = $this->timy_model->nacitaj_timy_podla_id_usera($id_usera);
			$this->data['moje_timy'] = $this->timy_model->nacitaj_timy_kde_som_admin($id_usera);
			$this->data['udalosti']  = $this->udalosti_model->nacitaj_udalosti_podla_id_usera($id_usera);
			
			if ($this->data['user_info']['foto'] != 'default.jpg'){
				$this->data['user_info']['foto'] = $this->uri->segment(2).'/'.$this->data['user_info']['foto'];
			}
			
			if (count($this->data['udalosti']) > 0){
				for ($i = 0; $i < count($this->data['udalosti']); $i++){
					$this->data['udalosti'][$i]['pocet_ucastnikov'] = $this->udalosti_model->nacitaj_pocet_ucastnikov_udalosti($this->data['udalosti'][$i]['id']);
				}
			}
			
			if (count($this->data['timy']) > 0){
				for ($t = 0; $t < count($this->data['timy']); $t++){
					$this->data['timy'][$t]['pocet_clenov'] = $this->timy_model->nacitaj_pocet_clenov_timu($this->data['timy'][$t]['id']);					
				}
			}
			
			if (isset($this->data['user_info']['email'])){
				if ($this->uri->segment(2) == $this->session->userdata('id')){
					$this->data['moznost_uprav'] = true;
				}else{
					$this->data['moznost_uprav'] = false;
				}
				
				$this->load->view('profil_view', $this->data);
			}else{
				$this->data['error_text'] = 'Užívateľ neexistuje.';
				
				$this->load->view('error_view', $this->data);
			}
		}
		
		function uloz_foto(){
			if ($this->session->userdata('prihlaseny')){
				if($_FILES['profil_foto']['tmp_name']){
					$this->_upload($this->session->userdata('id'));
				}else{
					$this->session->set_flashdata('error', 'Nebola vybraná žiada fotografia!');
					redirect('profil/'.$this->session->userdata('id'));
				}
			}	
		}
		
		function zmena_hesla(){
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>'); 
				$this->form_validation->set_rules('pass1', 'Heslo 1', 'required|min_length[5]|matches[pass2]');
				$this->form_validation->set_rules('pass2', 'Heslo 2', 'required');
				
				
				if ($this->form_validation->run()){
					$heslo 		=  $this->input->post('pass1');
					$this->profil_model->uloz_heslo($heslo, $this->session->userdata('id'));
					
					echo true;
				}else{
					echo '<p class="err_msg">Heslo NEBOLO zmenené!</p> '.validation_errors();
				}
			}
		}
		
		function zmena_meno_submit() {
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>'); 
				$this->form_validation->set_rules('meno', 'Meno', 'trim|required|min_length[3]');
				
				if ($this->form_validation->run()){
					$meno 		=  $this->input->post('meno');
					$this->profil_model->uloz_meno($meno, $this->session->userdata('id'));
					
					echo (empty($meno) ? "-" : $meno);
				}else{
					echo '<p class="err_msg">Meno NEBOLO zmenené!</p> '.validation_errors();
				}			
			}
		}
		
		function zmena_priezvisko_submit() {
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>'); 
				$this->form_validation->set_rules('priezvisko', 'Priezvisko', 'trim');
				
				if ($this->form_validation->run()){
					$priezvisko 		=  $this->input->post('priezvisko');
					$this->profil_model->uloz_priezvisko($priezvisko, $this->session->userdata('id'));
					
					echo (empty($priezvisko) ? "-" : $priezvisko);
				}else{
					echo '<p class="err_msg">Priezvisko NEBOLO zmenené!</p> '.validation_errors();
				}			
			}
		}
		
		function zmena_email_submit() {
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>'); 
				$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|required|is_unique[users.email]');
				
				if ($this->form_validation->run()){
					$email 		=  $this->input->post('email');
					$this->profil_model->uloz_email($email, $this->session->userdata('id'));
					echo $email;
				}else{
					echo '<p class="err_msg">Email NEBOL zmenený!</p> '.validation_errors();
				}			
			}
		}
		
		function zmena_datumu_submit() {
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>'); 
				$this->form_validation->set_rules('datum_nar', 'date', 'trim|valid_date');
				
				if ($this->form_validation->run()){
					if (($this->input->post('datum_nar') != '0000-00-00') && ($this->input->post('datum_nar'))){
						$datum 		=  date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('datum_nar'))));
						$this->profil_model->uloz_datum_nar($datum, $this->session->userdata('id'));
						echo date('d.m.Y',  strtotime($datum));
					}else{
						$datum 		= false;
						$this->profil_model->uloz_datum_nar($datum, $this->session->userdata('id'));	
						echo "-";	
					}	
				}else{
					echo '<p class="err_msg">Dátum NEBOL zmenený!</p> '.validation_errors();
				}		
			}
		}
		
		function zmena_vysky_submit() {
			if ($this->session->userdata('prihlaseny')){
				$vyska 		= $this->input->post('vyska');
				
				if (is_numeric($vyska) && ($vyska >= 0) && ($vyska < 1000)){
					$this->profil_model->uloz_vysku($vyska, $this->session->userdata('id'));
					
					echo $vyska.' cm';					
				}else{
					$vyska = 0;
					$this->profil_model->uloz_vysku($vyska, $this->session->userdata('id'));
					
					echo $vyska.' cm';	
				}							
			}
		}
		
		function zmena_vahy_submit() {
			if ($this->session->userdata('prihlaseny')){
				$vaha 		= $this->input->post('vaha');
				if (is_numeric($vaha) && ($vaha >= 0) && ($vaha < 1000)){
						
				}else{
					$vaha = 0;
				}			
				$this->profil_model->uloz_vahu($vaha, $this->session->userdata('id'));
					
				echo $vaha.' kg';
			}
		}
		
		function vytvor_team(){
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_rules('nazov', 'Pole názov', 'trim|required');
				$this->form_validation->set_rules('popis', 'Popis', 'trim|htmlspecialchars');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>');
				
				if ($this->form_validation->run()){
					$this->data['zobraz_form_vytvorenie_timu'] = false;
					
					$this->timy_model->pridaj_clena_timu($this->timy_model->pridaj_tim(), $this->session->userdata('id'), 1);
					
					$this->session->set_flashdata('message', 'Tím vytvorený!');
					
					redirect('profil/'.$this->session->userdata('id'));	
					exit;
				}else{
					$this->data['zobraz_form_vytvorenie_timu'] = true;
				}
								
				$this->zobraz_profil_usera($this->session->userdata('id'));
			}else{
				redirect('/home/');
			}
		}
		
		function vytvor_zapas(){
			if ($this->session->userdata('prihlaseny')){
				$this->load->library('form_validation');
				$this->form_validation->set_rules('nazov', 'Pole názov', 'trim|required');
				$this->form_validation->set_rules('miesto', 'Pole miesto', 'trim|required');
				$this->form_validation->set_rules('datum_konania', 'date', 'trim|required|valid_date');
				$this->form_validation->set_rules('cas_udalosti', 'cas', 'trim|required|valid_time');
				$this->form_validation->set_rules('popis', 'Popis', 'trim|htmlspecialchars');
				$this->form_validation->set_error_delimiters('<p class="err_msg">', '</p>');
				
				
				if ($this->form_validation->run()){
					$this->data['zobraz_form_vytvorenie_zapasu'] = false;
					
					$this->udalosti_model->pridaj_ucastnika($this->udalosti_model->pridaj_zapas(), $this->session->userdata('id'), 1);	
					
					$this->session->set_flashdata('message', 'Udalosť bola vytvorená!');
					
					redirect('profil/'.$this->session->userdata('id'));	
				}else{
					$this->data['zobraz_form_vytvorenie_zapasu'] = true;
				}
								
				$this->zobraz_profil_usera($this->session->userdata('id'));
			
			}else{
				redirect('/home/');
			}	
		}
		
		function ban_usera(){			
			if ($this->session->userdata('prihlaseny')){
				if ($this->uri->segment(2) != $this->session->userdata('id')){
					if ($this->session->userdata('admin') == 1){
						if ($this->profil_model->existuje_user($this->uri->segment(2))){
							if ($this->timy_model->zadal_spravne_heslo($this->session->userdata('id'), $_POST['potvrdzovacie_heslo'])){
								$this->profil_model->zabanuj_usera($this->uri->segment(2));
							
								$this->session->set_flashdata('message', 'Užívateľ bol zablokovaný!');
								
								redirect('/hraci/'); 
							}else{
								$this->session->set_flashdata('error', 'Nesprávne heslo!');						
							}
						}
					}
				}
			}	
			redirect('/profil/'.$this->uri->segment(2)); 
		} 
	}
?>