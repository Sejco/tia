<?php 
	class Hraci extends CI_Controller{
		
		function __construct(){
			parent::__construct(); 	
			$this->data['hraci'] = [];
			
			$this->load->model('hraci_model');
			$this->data['title']			 = 'Teamer - Hráči';
		}
		
		function index(){	
			$pole_hracov = $this->hraci_model->nacitaj_vsetkych_hracov();
			
			
			$this->data['hraci'] = $pole_hracov;
		
			$this->load->view('hraci_view', $this->data);			
		}
		
		function hladat_hraca(){
			$pole_hracov = $this->hraci_model->nacitaj_hladanych_hracov($this->security->xss_clean($this->input->get('hraci_search')));
			
			$this->data['hraci'] = $pole_hracov;
			
			$this->load->view('hraci_view', $this->data);
		}
	}
?>