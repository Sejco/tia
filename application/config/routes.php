<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 				= 'home';
$route['404_override'] 						= 'home';
$route['translate_uri_dashes'] 				= FALSE;

$route['profil/(:num)/ban_usera'] 			= 'profil/ban_usera';
$route['profil/(:num)/vytvor_zapas'] 		= 'profil/vytvor_zapas';
$route['profil/(:num)/vytvor_team'] 		= 'profil/vytvor_team';
$route['profil/(:num)'] 					= 'profil/zobraz_profil_usera/$1';

$route['udalosti/hladat'] 					= 'udalosti/hladat_udalost';
$route['udalosti/(:num)'] 					= 'udalosti/ukaz_detail_udalosti/$1';
$route['udalosti/(:num)/pridat_sa/(:num)'] 	= 'udalosti/pridaj_usera_k_udalosti/$1/$2';
$route['udalosti/(:num)/zrusit_ucast'] 		= 'udalosti/zrus_ucast/$1';
$route['udalosti/(:num)/zrusit_udalost'] 	= 'udalosti/zrusit_udalost/$1';
$route['udalosti/(:num)/zmen_popis'] 		= 'udalosti/zmen_popis/$1';
$route['udalosti/(:num)/pridaj_koment'] 	= 'udalosti/pridaj_koment/$1';
$route['udalosti/(:num)/del_koment/(:num)'] = 'udalosti/del_koment/$1/$2';

$route['timy/hladat'] 						= 'timy/hladat_tim';
$route['timy/schvalenie/(:num)/(:num)'] 	= 'timy/schval_clena/$1/$2';
$route['timy/vyhodenie/(:num)/(:num)'] 		= 'timy/vyhod_clena_z_timu/$1/$2';
$route['timy/(:num)/zrusit_tim'] 			= 'timy/zrusit_tim/$1';
$route['timy/(:num)/opustit_tim'] 			= 'timy/opustit_tim/$1';
$route['timy/(:num)/podat_prihlasku'] 		= 'timy/podat_prihlasku/$1';
$route['timy/(:num)/zrusit_prihlasku'] 		= 'timy/zrusit_prihlasku/$1';
$route['timy/(:num)/zmen_popis'] 			= 'timy/zmen_popis/$1';
$route['timy/(:num)'] 						= 'timy/ukaz_detail_timu/$1';

$route['hraci/hladat'] 						= 'hraci/hladat_hraca';
$route['hraci'] 							= 'hraci';

$route['password'] 							= 'auth/password_recovery';
$route['login'] 							= 'auth/login';
$route['logout'] 							= 'auth/logout';

$route['registracia'] 						= 'auth/registracia';


