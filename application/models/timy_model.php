<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

	class Timy_model extends CI_model{
		
		function nacitaj_vsetky_timy(){
			$select = $this->db->select('id, nazov') 
							   ->get('timy');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}	
		
		function existuje_tim($id_timu){
			$select = $this->db->select('*')    
							   ->from('timy')
							   ->where('id', $id_timu)				   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function nacitaj_tim_podla_id($id_timu){
			$select = $this->db->select('timy.*, users.meno, users.priezvisko')    
							   ->from('timy')	
							   ->join('users', 'timy.id_admina = users.id')
							   ->where('timy.id', $id_timu)							   
							   ->get();
							   
			if ($select->num_rows() > 0){	
				return $select->result_array()[0];
			}	
			return array();
		}
		
		function nacitaj_nazov_timu($id_timu){
			$select = $this->db->select('timy.nazov as nazov_timu')    
							   ->from('timy')	
							   ->where('timy.id', $id_timu)							   
							   ->get();
							   
			if ($select->num_rows() > 0){	
				return $select->result_array()[0]['nazov_timu'];
			}	
			return array();
		}
		
		function je_clenom_timu_user($id_usera, $id_timu){
			$select = $this->db->select('*')    
							   ->from('clenovia')
							   ->where('clenovia.id_timu', $id_timu)
							   ->where('clenovia.id_usera', $id_usera)
							   ->where('potvrdeny', 1)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function ma_podanu_prihlasku_user($id_usera, $id_timu){
			$select = $this->db->select('*')    
							   ->from('clenovia')
							   ->where('clenovia.id_timu', $id_timu)
							   ->where('clenovia.id_usera', $id_usera)	
							   ->where('potvrdeny', 0)
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function je_admin_timu($id_usera, $id_timu){
			$select = $this->db->select('*')    
							   ->from('timy')
							   ->where('timy.id_admina', $id_usera)
							   ->where('timy.id', $id_timu)	
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function nacitaj_clenov_timu($id_timu){
			$select = $this->db->select('clenovia.*, users.meno, users.priezvisko')    
							   ->from('clenovia')
							   ->join('users', 'clenovia.id_usera = users.id')
							   ->where('clenovia.id_timu', $id_timu)
							   ->order_by('clenovia.potvrdeny', 'desc')
							   ->order_by('clenovia.id', 'asc') 							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();	
		}

		function nacitaj_timy_podla_id_usera($id_usera){
			$select = $this->db->select('timy.*')    
							   ->from('clenovia')
							   ->join('timy', 'clenovia.id_timu = timy.id')	
							   ->where('clenovia.id_usera', $id_usera)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}	
		
		function nacitaj_timy_kde_som_admin($id_usera){
			$select = $this->db->select('timy.*')    
							   ->from('timy')
							   ->where('timy.id_admina', $id_usera)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_pocet_clenov_timu($id_timu){
			$select = $this->db->select('count(clenovia.id) as pocet_clenov')    
							   ->from('clenovia')
							   ->where('clenovia.id_timu', $id_timu)
							   ->where('clenovia.potvrdeny', 1)	
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->row_array()['pocet_clenov'];
			}	
			return "0";	
		}
		
		function pridaj_tim(){
			$data = array(
				'nazov' 		=> $_POST['nazov'],
				'id_admina' 	=> $this->session->userdata('id'),
				'popis'			=> $_POST['popis']
			);
			
			$this->db->insert('timy', $data);
			
			return $this->db->insert_id();
		}
		
		function pridaj_clena_timu($id_timu, $id_usera, $schvaleny){
			$data = array(
				'id_timu' 	=> $id_timu,
				'id_usera' 	=> $id_usera,
				'potvrdeny'	=> $schvaleny
			);
			
			return $this->db->insert('clenovia', $data);
		}
		
		function nacitaj_udalosti_timu($id_timu){
			$select = $this->db->select('*')    
							   ->from('udalosti')
							   ->where('udalosti.verejne', $id_timu)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_hladane_timy($vyraz){
			$select = $this->db->select('id, nazov') 
							   ->like('nazov', $vyraz)	
							   ->get('timy');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function schval_clena_timu($id_timu, $id_usera){
			$data = array(
				'potvrdeny' => 1
			);
			
			$this->db->where('id_timu', $id_timu)
					 ->where('id_usera', $id_usera)
					 ->update('clenovia', $data);
			
		}
		
		function vyhod_clena_timu($id_timu, $id_usera){
			$this->db->where('id_timu', $id_timu)
					 ->where('id_usera', $id_usera)
					 ->delete('clenovia');
		}
		
		function zmen_popis_timu($id_timu, $popis){
			$data = array(
				'popis' => $popis
			);
			
			$this->db->where('id', $id_timu)
					 ->update('timy', $data);		
		}
		
		function vyhod_vsetkych_clenov_timu($id_timu){
			$this->db->where('id_timu', $id_timu)
					 ->delete('clenovia');
		}
		
		function zmaz_tim($id_timu){
			$this->db->where('id', $id_timu)
					 ->delete('timy');
		}
		
		function zadal_spravne_heslo($id_usera, $heslo){
			$select = $this->db->select('*')
							   ->where('id', $id_usera) 
							   ->where('heslo', sha1($heslo))
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}

	}
?>