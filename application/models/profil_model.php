<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

	class Profil_model extends CI_model{
		
		function nacitaj_data_usera($id){
			$select = $this->db->select('id, meno, priezvisko, email, foto, datum_nar, vyska, vaha')
							   ->where('id', $id) 
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->row_array();
			}	
			return false;
		}
		
		function existuje_user($id_usera){
			$select = $this->db->select('*')    
							   ->from('users')
							   ->where('id', $id_usera)				   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function zabanuj_usera($id_usera){
			$data = array(
               'heslo' => '0'
            );

			$this->db->where('id', $id_usera);
			$this->db->update('users', $data); 
		}
		
		function uloz_meno($meno, $id){
			$data = array(
               'meno' => $meno
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_heslo($heslo, $id){
			$data = array(
               'heslo' => sha1($heslo)
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_priezvisko($priezvisko, $id){
			$data = array(
               'priezvisko' => $priezvisko
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_email($email, $id){
			$data = array(
               'email' => $email
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_datum_nar($datum, $id){
			$data = array(
               'datum_nar' => $datum
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_vysku($vyska, $id){
			$data = array(
               'vyska' => $vyska
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function uloz_vahu($vaha, $id){
			$data = array(
               'vaha' => $vaha
            );

			$this->db->where('id', $id);
			$this->db->update('users', $data); 			
		}
		
		function pridaj_obrazok_userovi($id_usera, $foto){
			$data = array(
               'foto' => $foto
            );

			$this->db->where('id', $id_usera);
			$this->db->update('users', $data);
		}
		
	}
?>