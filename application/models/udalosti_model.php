<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

	class Udalosti_model extends CI_model{
		
		function pridaj_zapas(){
			$data = array(
				'nazov' 	=> $_POST['nazov'],
				'popis' 	=> $_POST['popis'],
				'miesto'	=> $_POST['miesto'],
				'datum'		=> date('Y-m-d', strtotime($_POST['datum_konania'])),
				'cas'		=> date('H:i', strtotime($_POST['cas_udalosti'])),
				'id_admina'	=> $this->session->userdata('id'),
				'typ'		=> $_POST['typ_zapasu'],
				'verejne'	=> $_POST['verejne']	
			);
			
			$this->db->insert('udalosti', $data);
			
			return $this->db->insert_id();
		}
		
		function existuje_udalost($id_udalosti){
			$select = $this->db->select('*')    
							   ->from('udalosti')
							   ->where('id', $id_udalosti)				   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function je_adminom_udalosti($id_usera, $id_udalosti){
			$select = $this->db->select('*')    
							   ->from('udalosti')
							   ->where('id', $id_udalosti)				   
							   ->where('id_admina', $id_usera)				   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function zadal_spravne_heslo($id_usera, $heslo){
			$select = $this->db->select('*')
							   ->where('id', $id_usera) 
							   ->where('heslo', sha1($heslo))
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function zmen_popis_udalosti($id_udalosti, $popis){
			$data = array(
				'popis' => $popis
			);
			
			$this->db->where('id', $id_udalosti)
					 ->update('udalosti', $data);	
		}
		
		function vyhod_ucastnikov_udalosti($id_udalosti){
			$this->db->where('id_zapasu', $id_udalosti)
					 ->delete('ucastnici');
		}
		
		function zmaz_udalost($id_udalosti){
			$this->db->where('id', $id_udalosti)
					 ->delete('udalosti');
		}
		
		function pridaj_ucastnika($id_zapasu, $id_usera, $stav){
			$data = array(
				'id_zapasu' => $id_zapasu,
				'id_usera' 	=> $id_usera,
				'ucast' 	=> $stav
			);		
			return $this->db->insert('ucastnici', $data);
		}
		
		function zmen_stav_ucastnika($id_zapasu, $id_usera, $stav){
			$data = array(
				'ucast' 	=> $stav
			);		
			return $this->db->where('id_zapasu', $id_zapasu)
							->where('id_usera', $id_usera)
							->update('ucastnici', $data);
		}
		
		function vloz_komentar($id_udalosti, $id_usera, $koment){
			$data = array(
				'id_udalosti' => $id_udalosti,
				'id_usera' 	 => $id_usera,
				'komentar' 	 => $koment
			);		
			return $this->db->insert('komentare', $data);
		}
		
		function nacitaj_komentare_udalosti($id_udalosti){
			$select = $this->db->select('komentare.*, users.meno, users.priezvisko')
							   ->from('komentare')	
							   ->join('users', 'komentare.id_usera = users.id')
							   ->where('komentare.id_udalosti', $id_udalosti)
							   ->order_by('komentare.cas', 'asc')
							   ->get();							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function odober_ucastnika($id_zapasu, $id_usera){
			$this->db->where('id_zapasu', $id_zapasu)
					 ->where('id_usera', $id_usera)
					 ->delete('ucastnici');
		}
		
		function je_ucatnikom_udalosti($id_udalosti, $id_usera){
			$select = $this->db->select('*')    
							   ->from('ucastnici')
							   ->where('ucastnici.id_zapasu', $id_udalosti)
							   ->where('ucastnici.id_usera', $id_usera)						   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		}
		
		function nacitaj_vsetky_udalosti(){
			$select = $this->db->select('*') 
							   ->order_by('datum', 'desc')						   
						       ->order_by('cas', 'desc') 			
							   ->get('udalosti');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_hladane_udalosti($vyraz){
			$select = $this->db->select('*') 		
						   ->like('nazov', $vyraz)	
						   ->or_like('miesto', $vyraz)	
						   ->or_like('datum', $vyraz)		
						   ->or_like('cas', $vyraz)	
						   ->order_by('datum', 'desc')						   
						   ->order_by('cas', 'desc') 						   
						   ->get('udalosti');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_pocet_ucastnikov_udalosti($id_udalosti){
			$ucast_pole = array(1, 2);
			$select = $this->db->select('count(ucastnici.id) as pocet_ucastnikov')    
							   ->from('ucastnici')
							   ->where('ucastnici.id_zapasu', $id_udalosti)
							   ->where_in('ucastnici.ucast', $ucast_pole)
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->row_array()['pocet_ucastnikov'];
			}	
			return "0";
		}
		
		function nacitaj_udalosti_podla_id_usera($id_usera){
			$select = $this->db->select('udalosti.*')    
							   ->from('ucastnici')
							   ->join('udalosti', 'ucastnici.id_zapasu = udalosti.id')	
							   ->where('ucastnici.id_usera', $id_usera)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_ucastnikov_udalosti($id_udalosti){
			$select = $this->db->select('ucastnici.*, users.meno, users.priezvisko')    
							   ->from('ucastnici')
							   ->join('users', 'ucastnici.id_usera = users.id')
							   ->where('ucastnici.id_zapasu', $id_udalosti)	
							   ->order_by('ucast', 'asc')	
							   ->order_by('meno', 'asc')	
							   ->order_by('priezvisko', 'asc')	
							   ->get();
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_moj_stav($id_udalosti, $id_usera){
			$select = $this->db->select('ucastnici.ucast') 
							   ->where('id_zapasu', $id_udalosti) 			
							   ->where('id_usera', $id_usera) 			
							   ->get('ucastnici');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array()[0]['ucast'];
			}	
			return array();
		}
		
		function nacitaj_udalost_podla_id($id_udalosti){
			$select = $this->db->select('udalosti.*, users.meno, users.priezvisko')
							   ->from('udalosti')
							   ->join('users', 'udalosti.id_admina = users.id')
						       ->where('udalosti.id', $id_udalosti)						   
						       ->get();							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array()[0];
			}	
			return array();
		}
		
		function som_v_time_ktory_organizuje_udalost($id_usera, $id_timu){
			$select = $this->db->select('*')    
							   ->from('clenovia')
							   ->where('clenovia.id_timu', $id_timu)
							   ->where('clenovia.id_usera', $id_usera)
							   ->where('potvrdeny', 1)							   
							   ->get();
					
			if ($select->num_rows() > 0){	
				return true;
			}	
			return false;
		} 
		
		function odstran_koment($id_komenu){
			$this->db->where('id', $id_komenu)
					 ->delete('komentare');
		}
		
	}
?>