<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

	class Auth_model extends CI_model{
	
		function check(){
			$select = $this->db->select('id, meno, priezvisko, email, admin')
							   ->where('email', $_POST['email']) 
							   ->where('heslo', sha1($_POST['pass']))
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->row_array();
			}	
			return false;
		}
		
		function register(){
			$data = array(
				'meno' 			=> $_POST['meno'],
				'priezvisko' 	=> $_POST['priezvisko'],
				'email'			=> $_POST['email'],
				'heslo' 		=> sha1($_POST['heslo'])
			);
			
			return $this->db->insert('users', $data);
		}
		
		function existuje_email($mail){
			$select = $this->db->select('id')
							   ->where('email', $mail) 
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->row_array();
			}	
			return false;	
		}
		
		function zmen_heslo_podla_mailu($heslo, $mail){
			$data = array(
               'heslo' => sha1($heslo)
            );

			$this->db->where('email', $mail);
			$this->db->update('users', $data); 
		}
		
	}
?>