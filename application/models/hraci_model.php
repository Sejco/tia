<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

	class Hraci_model extends CI_model{
		
		function nacitaj_vsetkych_hracov(){
			$select = $this->db->select('id, meno, priezvisko, email') 
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		function nacitaj_hladanych_hracov($vyraz){
			$select = $this->db->select('id, meno, priezvisko, email') 
							   ->like('meno', $vyraz)	
							   ->or_like('priezvisko', $vyraz)	
							   ->or_like('email', $vyraz)	
							   ->get('users');							   				   
					
			if ($select->num_rows() > 0){	
				return $select->result_array();
			}	
			return array();
		}
		
		

	}
?>