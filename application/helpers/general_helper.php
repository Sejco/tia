<?php
	function pre_r($d){
		echo "<pre>";
		print_r($d);
		echo "</pre>";
	}
	
	function valid_date($date){	
		$casti_datumu = array_filter(explode(".", $date));
		if (count($casti_datumu) == 3){
			if ((is_numeric($casti_datumu[0])) && (is_numeric($casti_datumu[1])) && (is_numeric($casti_datumu[2]))){
				if (checkdate($casti_datumu[1], $casti_datumu[0], $casti_datumu[2])){
					return true;
				}
			}
		}
		$casti_datumu = array_filter(explode("-", $date));
		if (count($casti_datumu) == 3){
			if ((is_numeric($casti_datumu[0])) && (is_numeric($casti_datumu[1])) && (is_numeric($casti_datumu[2]))){
				if ((checkdate($casti_datumu[1], $casti_datumu[0], $casti_datumu[2])) || (checkdate($casti_datumu[1], $casti_datumu[2], $casti_datumu[0]))){
					return true;
				}
			}
		}
		$casti_datumu = array_filter(explode("/", $date));
		if (count($casti_datumu) == 3){
			if ((is_numeric($casti_datumu[0])) && (is_numeric($casti_datumu[1])) && (is_numeric($casti_datumu[2]))){	
				if (checkdate($casti_datumu[1], $casti_datumu[0], $casti_datumu[2])){
					return true;
				}
			}
		}
		return false;		
	}
	
	function valid_time($time){
		if (preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $time)){
			return true;
		}
		return false;
	}
?>