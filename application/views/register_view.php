<?php $this->load->view('header'); ?>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container_header">
		<h1>Registrácia</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container">	
		<div class="sirka-200 center">			
			<?php
				$form['meno'] = array(
					'name' 			=> 'meno',
					'value'			=> $this->input->post('meno'),
					'class' 		=> 'form-control',
					'placeholder'	=> 'Meno',
					'required'      => 'required'
				);
				$form['priezvisko'] = array(
					'name' 			=> 'priezvisko',
					'value'			=> $this->input->post('priezvisko'),
					'class' 		=> 'form-control ',
					'placeholder'	=> 'Priezvisko',
					'required'      => 'required'
				);
				$form['email'] = array(
					'name' 			=> 'email',
					'value'			=> $this->input->post('email'),
					'class' 		=> 'form-control',
					'placeholder'	=> 'E-mail',
					'type'			=> 'email',
					'required'      => 'required'
				);
				$form['heslo']	= array(
					'name' 			=> 'heslo',
					'class' 		=> 'form-control',
					'placeholder'	=> 'Heslo',
					'required'      => 'required'
				);
				$form['submit'] = array(
					'name' 			=> 'submit',
					'value'			=> 'Zaregistrovať',
					'class'			=> 'btn btn-primary margin-top-25 margin-bottom-5 sirka-100p'
				);

				echo form_open('registracia');
					?>
						<table class="table table-borderless center xs-table-block">
							<tr>
								<td class="nopadding-left-right"><?=form_input($form['meno']);?></td>
							</tr>
							<tr>
								<td class="nopadding"><?=form_error('meno');?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_input($form['priezvisko']);?></td>
							</tr>
							<tr>
								<td class="nopadding"><?=form_error('priezvisko');?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_input($form['email']);?></td>
							</tr>
							<tr>
								<td class="nopadding"><?=form_error('email');?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_password($form['heslo']);?></td>
							</tr>
							<tr>
								<td class="nopadding"><?=form_error('heslo');?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_submit($form['submit']);?></td>
							</tr>	
								
						</table>
					<?php	
				echo form_close();
			?>
		</div>
	</div>

<?php $this->load->view('footer'); ?>
