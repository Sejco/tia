<?php $this->load->view('header'); ?>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 container_header">
		<h1><?=$detail_timu['nazov']?></h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 container">
		<?php
			if($this->session->flashdata('message')){
				?>
					<div id="flash-messages" class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('message');?>
					</div>
				<?php
			}elseif($this->session->flashdata('error')){
				?>
					<div id="flash-messages" class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('error');?>
					</div>
				<?php
			}
		?>
		
		<section class="margin-bottom-40">
			<h2>Informácie</h2>
			<table class="table table_supa_styl">
				<thead>
					<tr>
						<th colspan="2">
							<strong>Popis</strong>
							<?php
								if ($detail_timu['id_admina'] == $this->session->userdata('id')){
									?>
										<a href="javascript:void(0);">
											<span class="glyphicon glyphicon-pencil" title="Zmeniť popis" onclick="zobraz_textareu_popis_timu();"></span>
										</a>
									<?php
								}
							?>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2">
							<div id="text_popisu_timy" class="nomargin_p">
								<?=auto_typography($detail_timu['popis']);?>
							</div>
							<div id="textarea_popisu_timy" class="none">
								<form action="<?=base_url()?>timy/<?=$detail_timu['id']?>/zmen_popis" method="post" accept-charset="utf-8">
									<textarea name="popis_timu" class="form-control noresize margin-bottom-5" rows="10"><?=$detail_timu['popis'];?></textarea>
									<input type="submit" name="povrd_popis" value="Uložiť popis" class="btn btn-success">
									<button type="button" class="btn btn-danger" onclick="schovaj_textareu_popis_timu();">Zrušiť zmeny</button>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Zakladateľ</strong></td>
						<td><a href="<?= base_url()?>profil/<?=$detail_timu['id_admina']?>"><?=$detail_timu['meno'].' '.$detail_timu['priezvisko']?></a></td>
					</tr>
				</tbody>
			</table>
		</section>
		
		<section class="margin-bottom-40">			
			<h3>Udalosti</h3>
			<table class="table tablesorter table-hover table_supa_styl" id="myTable">
				<thead>
					<tr>
						<th>
							Názov
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Miesto
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Dátum
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Čas
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Účasť
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-zobraz">
							Dátum
							<span class="glyphicon glyphicon-sort"></span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (count($udalosti) > 0){
							for ($i = 0; $i < count($udalosti); $i++){
								?>
									<tr>
										<td>
											<?=(($udalosti[$i]['typ'] == 2) ? '<span class="glyphicon glyphicon-star" title="Zápas"></span>' : '<span class="glyphicon glyphicon-record" title="Tréning"></span>')?>
											<a href="/udalosti/<?=$udalosti[$i]['id']?>"><?=$udalosti[$i]['nazov']?></a>
										</td>
										<td class="xs-table-none"><?=$udalosti[$i]['miesto']?></td>
										<td class="xs-table-none"><?=date('d.m.Y', strtotime($udalosti[$i]['datum']))?></td>
										<td class="xs-table-none"><?=date('H:i', strtotime($udalosti[$i]['cas']))?></td>
										<td class="xs-table-none"><?=$udalosti[$i]['pocet_ucastnikov']?></td>
										<td class="xs-table-zobraz">
											<?php
												echo date('d.m.Y', strtotime($udalosti[$i]['datum'])).' '.date('H:i', strtotime($udalosti[$i]['cas']));
											?>
										</td>
									</tr>
								<?php
							}
						}else{
							?>
								<tr>
									<td colspan="5">Žiadne nájdené udalosti</td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</section>	
					

		<section class="margin-bottom-40">
			<h2>Členovia (<?=$pocet_clenov?>)</h2>
			<table class="table table-hover table_supa_styl">
				<thead>
					<tr>
						<th>Meno člena</th>
						<th>Možnosti</th>
					</tr>
				</thead>
				<tbody>
					<?php
						for ($i = 0; $i < count($clenovia); $i++){
							?>
								<tr>
									<td>
										<a href="<?= base_url()?>profil/<?=$clenovia[$i]['id_usera']?>">
										<span title="<?=($clenovia[$i]['potvrdeny'] == 1) ? 'Člen tímu' : 'Čaká na schválenie'?>" class="glyphicon <?=($clenovia[$i]['potvrdeny'] == 1) ? 'glyphicon glyphicon-ok-sign' : 'glyphicon glyphicon-question-sign'?>"></span>
										<?=$clenovia[$i]['meno'].' '.$clenovia[$i]['priezvisko']?>
										</a>
									</td>
									<td>
										<a href="<?= base_url()?>profil/<?=$clenovia[$i]['id_usera']?>" class="btn btn-primary btn-md">
											Profil <span class="glyphicon glyphicon-eye-open"></span>
										</a>
										<?php
											if ($je_admin_timu){
												if ($clenovia[$i]['potvrdeny'] == 1){
													if ($clenovia[$i]['id_usera'] != $this->session->userdata('id')){
														?>
															<a href="<?= base_url()?>timy/vyhodenie/<?=$detail_timu['id'].'/'.$clenovia[$i]['id_usera']?>" class="btn btn-danger btn-md pull-right">
																Zrušiť členstvo <span class="glyphicon glyphicon-trash"></span>
															</a>
														<?php
													}
												}else{
													?>
														<a href="<?= base_url()?>timy/schvalenie/<?=$detail_timu['id'].'/'.$clenovia[$i]['id_usera']?>" class="btn btn-success btn-md">
															Schváliť žiadosť <span class="glyphicon glyphicon-ok-circle"></span>
														</a>
														<a href="<?= base_url()?>timy/vyhodenie/<?=$detail_timu['id'].'/'.$clenovia[$i]['id_usera']?>" class="btn btn-danger btn-md pull-right">
															Zamietnuť žiadosť <span class="glyphicon glyphicon-remove-circle"></span>
														</a>
													<?php
												}
											}
										?>
									</td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</section>

		<?php
			if ($this->session->userdata('prihlaseny')){
				?>
					<section>
						<h2 class="margin-bottom-15">Možnosti tímu</h2>
						<table class="table table_supa_styl">
							<thead>
								<tr>
									<th>Možnosti</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if ((!$ma_prihlasku) && (!$je_clen)){
										?>
											<tr>
												<td>
													<a href="<?= base_url()?>timy/<?=$detail_timu['id']?>/podat_prihlasku" class="btn btn-success sirka-200">
														Podať prihlášku <span class="glyphicon glyphicon-play-circle"></span>
													</a>
												</td>
											</tr>	
										<?php
									}
									if (($je_clen) && (!$je_admin_timu)){
										?>
											<tr>
												<td>
													<a href="<?= base_url()?>timy/<?=$detail_timu['id']?>/opustit_tim" class="btn btn-danger sirka-200">
														Opustiť tím <span class="glyphicon glyphicon-trash"></span>
													</a>
												<td>
											<tr>
										<?php
									}
									if (($je_admin_timu) && (!$je_admin)){
										?>
											<tr>
												<td>
													<div id="cudlik_zrusenie_timu">
														<a href="javascript:void(0);" onclick="ukaz_form_potvrdenie_zrusenia_timu();" class="btn btn-danger sirka-200">
															Zrušiť tím <span class="glyphicon glyphicon-trash"></span>
														</a>
													</div>
													<div id="form_zrusenie_timu" class="none">
														<form action="<?= base_url()?>timy/<?=$detail_timu['id']?>/zrusit_tim" method="post" accept-charset="utf-8" class="form-inline">
															<label for="potvrdzovacie_heslo">Zadajte vaše heslo: </label>
															<input type="password" class="form-control" name="potvrdzovacie_heslo" id="potvrdzovacie_heslo">
															<button type="submit" class="btn btn-danger btn-md">
																Potvrdiť <span class="glyphicon glyphicon-trash"></span>
															</button>
															<button type="button" class="btn btn-danger btn-md" onclick="schovaj_form_potvrdenie_zrusenia_timu();">
																Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
															</button>
														</form>
													</div>
												</td>
											</tr>
										<?php
									}
									if ($ma_prihlasku){
										?>
											<tr>
												<td>
													<a href="<?= base_url()?>timy/<?=$detail_timu['id']?>/zrusit_prihlasku" class="btn btn-danger sirka-200">
														Zrušiť prihlášku <span class="glyphicon glyphicon-trash"></span>
													</a>
												</td>
											</tr>
										<?php
									}
									if ($je_admin){
										?>
											<tr>
												<td>
													<div id="cudlik_zrusenie_timu">
														<a href="javascript:void(0);" onclick="ukaz_form_potvrdenie_zrusenia_timu();" class="btn btn-danger sirka-200">
															Zrušiť tím <span class="glyphicon glyphicon-trash"></span>
														</a>
													</div>
													<div id="form_zrusenie_timu" class="none">
														<form action="<?= base_url()?>timy/<?=$detail_timu['id']?>/zrusit_tim" method="post" accept-charset="utf-8" class="form-inline">
															<label for="potvrdzovacie_heslo">Zadajte vaše heslo: </label>
															<input type="password" class="form-control" name="potvrdzovacie_heslo" id="potvrdzovacie_heslo">
															<button type="submit" class="btn btn-danger btn-md">
																Potvrdiť <span class="glyphicon glyphicon-trash"></span>
															</button>
															<button type="button" class="btn btn-danger btn-md" onclick="schovaj_form_potvrdenie_zrusenia_timu();">
																Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
															</button>
														</form>
													</div>
												</td>
											</tr>
										<?php
									}
								?>
							</tbody>
						</table>
					</section>
				<?php
			}
		?>
	</div>

<?php $this->load->view('footer'); ?>
