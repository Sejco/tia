<?php $this->load->view('header'); ?>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container_header">
		<h1>Hráči</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container">
		<div class="table-responsive">	
			<form action="<?= base_url()?>hraci/hladat/" class="form-inline margin-bottom-15 pull-right" method="get" accept-charset="utf-8">
				<div class="input-group">
					<input type="text" name="hraci_search" id="hraci_search" placeholder="Zadajte výraz" class="form-control" value="<?=$this->input->get('hraci_search')?>">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-search"></span> Vyhľadať
						</button>
					</div>
				 </div>
			</form>
			<?php
				if($this->session->flashdata('message')){
					?>
						<div id="flash-messages" class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<?=$this->session->flashdata('message');?>
						</div>
					<?php
				}elseif($this->session->flashdata('error')){
					?>
						<div id="flash-messages" class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<?=$this->session->flashdata('error');?>
						</div>
					<?php
				}
			?>
			<table class="table tablesorter table-hover table_supa_styl" id="myTable">
				<thead>
					<tr>
						<th>
							Meno a priezvisko
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th>
							E-mail
							<span class="glyphicon glyphicon-sort"></span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (count($hraci) > 0){
							for ($i = 0; $i < count($hraci); $i++){
								?>
									<tr>
										<td><a href="/profil/<?=$hraci[$i]['id']?>"><?=$hraci[$i]['meno'].' '.$hraci[$i]['priezvisko']?></a></td>
										<td><?=$hraci[$i]['email']?></td>
									</tr>
								<?php
							}
						}else{
							?>
								<tr>
									<td  colspan="2"><p>Nenašli sa žiadne výsledky.</p></td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

<?php $this->load->view('footer'); ?>