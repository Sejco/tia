<?php $this->load->view('header'); ?>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container_header">
		<h1><?=$udalost['nazov']?></h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container">
		<?php
			if($this->session->flashdata('message')){
				?>
					<div id="flash-messages" class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('message');?>
					</div>
				<?php
			}elseif($this->session->flashdata('error')){
				?>
					<div id="flash-messages" class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('error');?>
					</div>
				<?php
			}
		?>
		<section class="margin-bottom-40">
			<h2>Informácie</h2>
			<table class="table table_supa_styl">
				<thead>
					<tr>
						<th colspan="2">
							<strong>Popis</strong>
							<?php
								if ($udalost['id_admina'] == $this->session->userdata('id')){
									?>
										<a href="javascript:void(0);">
											<span class="glyphicon glyphicon-pencil" title="Zmeniť popis" onclick="schovaj('text_popisu_udalosti');ukaz('textarea_popisu_udalosti');"></span>
										</a>
									<?php
								}
							?>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2">
							<div id="text_popisu_udalosti" class="nomargin_p">
								<?=auto_typography($udalost['popis']);?>
							</div>
							<div id="textarea_popisu_udalosti" class="none">
								<form action="<?=base_url()?>udalosti/<?=$udalost['id']?>/zmen_popis" method="post" accept-charset="utf-8">
									<textarea name="popis_udalosti" class="form-control noresize margin-bottom-5" rows="10"><?=$udalost['popis'];?></textarea>
									<input type="submit" name="povrd_popis" value="Uložiť popis" class="btn btn-success">
									<button type="button" class="btn btn-danger" onclick="schovaj('textarea_popisu_udalosti');ukaz('text_popisu_udalosti');">Zrušiť zmeny</button>
								</form>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><strong>Organizátor</strong></td>
						<td><a href="<?= base_url()?>profil/<?=$udalost['id_admina']?>"><?=$udalost['meno'].' '.$udalost['priezvisko']?></a></td>
					</tr>
					<?php
						if ($udalost['verejne'] != 0){
							?>
								<tr>
									<td><strong>Tím</strong></td>
									<td><a href="<?= base_url()?>timy/<?=$udalost['verejne']?>"><?=$udalost['nazov_timu']?></a></td>
								</tr>
							<?php
						}
					?>
					<tr>
						<td><strong>Miesto konania</strong></td>
						<td><?=$udalost['miesto']?></td>
					</tr>
					<tr>
						<td><strong>Dátum</strong></td>
						<td><?=date('d.m.Y', strtotime($udalost['datum']))?></td>
					</tr>
					<tr>
						<td><strong>Čas</strong></td>
						<td><?=date('H:i', strtotime($udalost['cas']))?></td>
					</tr>
				</tbody>
			</table>
		</section>
		<?php
			if ($this->session->userdata('prihlaseny')){
				?>
					<section class="margin-bottom-40">
						<h2>Možnosti k udalosti</h2>
						<table class="table table_supa_styl">
							<thead>
								<tr>
									<th colspan="3">Možnosti</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if ($je_admin_udalosti){
										if (!$je_adminom){
											?>
												<tr>
													<td colspan="3">
														<div id="cudlik_zrusenie_udalosti">
															<a href="javascript:void(0);" onclick="ukaz('form_zrusenie_udalosti');schovaj('cudlik_zrusenie_udalosti');" class="btn btn-danger sirka-200">
																Zrušiť udalosť <span class="glyphicon glyphicon-trash"></span>
															</a>
														</div>
														<div id="form_zrusenie_udalosti" class="none">
															<form action="<?= base_url()?>udalosti/<?=$udalost['id']?>/zrusit_udalost" method="post" accept-charset="utf-8" class="form-inline">
																<label for="potvrdzovacie_heslo">Pre potvrdenie zadajte vaše heslo: </label><br />
																<input type="password" class="form-control" name="potvrdzovacie_heslo" id="potvrdzovacie_heslo">
																<button type="submit" class="btn btn-danger btn-md">
																	Potvrdiť <span class="glyphicon glyphicon-trash"></span>
																</button>
																<button type="button" class="btn btn-danger btn-md" onclick="schovaj('form_zrusenie_udalosti');ukaz('cudlik_zrusenie_udalosti');">
																	Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
																</button>
															</form>
														</div>
													</td>
												</tr>
											<?php
										}
									}else{
										if ($je_ucastnikom){
											?>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/1" class="btn btn-success sirka-200" <?=(($moj_stav == 1) ? 'disabled' : '')?>>
															Prídem <span class="glyphicon glyphicon-ok-sign"></span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/2" class="btn btn-warning sirka-200" <?=(($moj_stav == 2) ? 'disabled' : '')?>>
															Možno prídem <span class="glyphicon glyphicon-question-sign"></span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/3" class="btn btn-danger sirka-200" <?=(($moj_stav == 3) ? 'disabled' : '')?>>
															Nezúčastním sa <span class="glyphicon glyphicon-remove-sign"></span>
														</a>
													</td>
												</tr>
											<?php
										}else{
											?>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/1" class="btn btn-success sirka-200">
															Prídem <span class="glyphicon glyphicon-ok-sign"></span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/2" class="btn btn-warning sirka-200">
															Možno prídem <span class="glyphicon glyphicon-question-sign"></span>
														</a>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<a href="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridat_sa/3" class="btn btn-danger sirka-200">
															Nezúčastním sa <span class="glyphicon glyphicon-remove-sign"></span>
														</a>
													</td>
												</tr>
											<?php
										}
									}
									if ($je_adminom){
										?>
											<tr>
												<td colspan="3">
													<div id="cudlik_zrusenie_udalosti">
														<a href="javascript:void(0);" onclick="ukaz('form_zrusenie_udalosti');schovaj('cudlik_zrusenie_udalosti');" class="btn btn-danger sirka-200">
															Zrušiť udalosť <span class="glyphicon glyphicon-trash"></span>
														</a>
													</div>
													<div id="form_zrusenie_udalosti" class="none">
														<form action="<?= base_url()?>udalosti/<?=$udalost['id']?>/zrusit_udalost" method="post" accept-charset="utf-8" class="form-inline">
															<label for="potvrdzovacie_heslo">Pre potvrdenie zadajte vaše heslo: </label><br />
															<input type="password" class="form-control" name="potvrdzovacie_heslo" id="potvrdzovacie_heslo">
															<button type="submit" class="btn btn-danger btn-md">
																Potvrdiť <span class="glyphicon glyphicon-trash"></span>
															</button>
															<button type="button" class="btn btn-danger btn-md" onclick="schovaj('form_zrusenie_udalosti');ukaz('cudlik_zrusenie_udalosti');">
																Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
															</button>
														</form>
													</div>
												</td>
											</tr>
										<?php
									}
								?>
							</tbody>
						</table>
					</section>
				<?php
			}
		?>
		<section class="margin-bottom-40">
			<h2>Účastníci (<?=$pocet_ucastnikov?>)</h2>
			<table class="table tablesorter table-hover table_supa_styl" id="myTable">
				<thead>
					<tr>
						<th>
							Meno účastníka
							<span class="glyphicon glyphicon-sort"></span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						for ($i = 0; $i < count($ucastnici); $i++){
							?>
								<tr>
									<td>
										<?php
											if ($ucastnici[$i]['ucast'] == 1){
												?>
													<span class="glyphicon glyphicon-ok-sign green"></span>
												<?php
											}else if($ucastnici[$i]['ucast'] == 2){
												?>
													<span class="glyphicon glyphicon-question-sign yellow"></span>
												<?php
											}else{
												?>
													<span class="glyphicon glyphicon-remove-sign red"></span>
												<?php
											}
										?>
										<a href="<?= base_url()?>profil/<?=$ucastnici[$i]['id_usera']?>"><?=$ucastnici[$i]['meno'].' '.$ucastnici[$i]['priezvisko']?></a>
									</td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</section>
		<section>
			<h2>Komentáre (<?=count($komentare)?>)</h2>
			<table class="table table-borderless table-hover table_supa_styl">
				<thead>
					<tr>
						<th colspan="2">Najnovšie</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if (count($komentare) == 0){
						?>
							<tr>
								<td colspan="2">Žiadne komentáre. Buďte prvý.</td>
							</tr>
						<?php
					}else{
						for ($i = 0; $i < count($komentare); $i++){
							?>
								<tr>
									<td class="nopadding-bottom">
										<a href="<?= base_url()?>profil/<?=$komentare[$i]['id_usera']?>"><?=$komentare[$i]['meno'].' '.$komentare[$i]['priezvisko']?></a>
										<?php
											if ($this->session->userdata('admin') == 1){
												?>
													<div id="ikona_del_koment_<?=$i?>" class="inline-block">
														<a href="javascript:void(0);" title="Vymazať komentár" onclick="schovaj('ikona_del_koment_<?=$i?>'); ukaz('cudlik_del_koment_<?=$i?>');">
															<span class="glyphicon glyphicon-trash"></span>
														</a>
													</div>
												<?php
											}
										?>
									</td>
									<td class="nopadding-bottom">
										<p class="pull-right nomargin"><?=date('d.m.Y  H:i', strtotime($komentare[$i]['cas']))?></p>
									</td>
								</tr>
								<?php
									if ($this->session->userdata('admin') == 1){
										?>
											<tr>
												<td colspan="2" class="nopadding-top-bot">
													<div id="cudlik_del_koment_<?=$i?>" class="none">
														<form action="<?= base_url()?>udalosti/<?=$udalost['id']?>/del_koment/<?=$komentare[$i]['id']?>" method="post" accept-charset="utf-8" class="form-inline">
															<button type="submit" class="btn btn-danger btn-md">
																Potvrdiť <span class="glyphicon glyphicon-trash"></span>
															</button>
															<button type="button" class="btn btn-danger btn-md" onclick="ukaz('ikona_del_koment_<?=$i?>'); schovaj('cudlik_del_koment_<?=$i?>');">
																Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
															</button>
														</form>
													</div>
												</td>
											</tr>
										<?php
									}
								?>
								<tr>
									<td colspan="2" class="<?=(($i < (count($komentare) - 1)) ? 'border_bottom' : '')?>">
										<?=auto_typography($komentare[$i]['komentar']);?>
									</td>
								</tr>
							<?php
						}
					}
				?>
				</tbody>
			</table>
			<?php
				if ($this->session->userdata('prihlaseny')){
					?>
						<form action="<?= base_url()?>udalosti/<?=$udalost['id']?>/pridaj_koment" method="post">
							<textarea class="form-control noresize margin-bottom-5" name="text_komentu" rows="4" placeholder="Zanechajte odkaz..." required></textarea>
							<button type="submit" class="btn btn-success btn-md">
								Odoslať
								<span class="glyphicon glyphicon-arrow-right"></span>
							</button>
						</form>
					<?php
				}
			?>
		</section>
	</div>

<?php $this->load->view('footer'); ?>