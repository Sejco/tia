<?php $this->load->view('header');?>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container_header">
		<h1><?=$user_info['meno'].' '.$user_info['priezvisko']?></h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container">

		<?php
			if($this->session->flashdata('message')){
				?>
					<div id="flash-messages" class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('message');?>
					</div>
				<?php
			}elseif($this->session->flashdata('error')){
				?>
					<div id="flash-messages" class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('error');?>
					</div>
				<?php
			}
		?>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding">
			<table class="table table_supa_styl">
				<thead>
					<tr>
						<th>Foto</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<img src="../../images/<?=$user_info['foto']?>" alt="<?=$user_info['meno']?>" class="img-responsive sirka-100p foto-profilu">
						</td>
					</tr>
					<?php
						if ($moznost_uprav){
							?>
								<tr>
									<td>
										<div id="cudlik_profilovka" class="center-text">
											<a href="javascript:void(0);" onclick="ukaz('form_profilovka');schovaj('cudlik_profilovka');">Zmeniť profilovú fotku <span class="glyphicon glyphicon-pencil" title="Zmeniť fotku"></span></a>
										</div>
										<div id="form_profilovka" class="none">
											<?php
												$attributes = array('class' => 'form-inline');
												echo form_open_multipart('profil/uloz_foto', $attributes);
													?>
														<input class="btn btn-primary sirka-100p margin-bottom-5" type="file" id="profil_foto" name="profil_foto">

														<button type="submit" class="btn btn-success">
															Uložiť
															<span class="glyphicon glyphicon-ok"></span>
														</button>
														<button type="button" class="btn btn-danger pull-right" onclick="ukaz('cudlik_profilovka');schovaj('form_profilovka');">
															Zrušiť
															<span class="glyphicon glyphicon-remove"></span>
														</button>
													<?php
												echo form_close();
											?>
										</div>
									</td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 xs-padding-sides-none">
			<table class="table profil-tabulka table_supa_styl">
				<thead>
					<tr>
						<th colspan="2">Informácie</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="col-xs-6 col-sm-4 col-md-4 col-lg-4"><strong>Meno</strong></td>
						<td class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
							<div id="meno_err" class="inline-block none">
							
							</div>
							<div id="meno_text" class="inline-block">
								<?php
									if (!$user_info['meno']){
										echo '-';
									}else{
										echo $user_info['meno'];
									}
								?>
							</div>
							<div id="meno_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="text" name="meno" id="meno" class="form-control" value="<?=$user_info['meno']?>"><br />
									<button type="submit" name="povrd_meno" id="profil_submit_meno" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="nastav_val('meno', $('#meno_text').text().trim()); schovaj('meno_err'); schovaj('meno_form'); ukaz('meno_edit'); ukaz('meno_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="meno_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť meno" onclick="ukaz('meno_form'); schovaj('meno_text'); schovaj('meno_edit')"></span>
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Priezvisko</strong></td>
						<td>
							<div id="priezvisko_err" class="inline-block none">
							
							</div>
							<div id="priezvisko_text" class="inline-block">
								<?php
									if (!$user_info['priezvisko']){
										echo '-';
									}else{
										echo $user_info['priezvisko'];
									}
								?>
							</div>
							<div id="priezvisko_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="text" name="priezvisko" id="priezvisko" class="form-control" value="<?=$user_info['priezvisko']?>">
									<button type="submit" name="povrd_priezvisko" id="profil_submit_priezvisko" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="nastav_val('priezvisko', $('#priezvisko_text').text().trim()); schovaj('priezvisko_err'); schovaj('priezvisko_form'); ukaz('priezvisko_edit'); ukaz('priezvisko_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="priezvisko_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť Priezvisko" onclick="ukaz('priezvisko_form'); schovaj('priezvisko_text'); schovaj('priezvisko_edit');"></span>
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>E-mail</strong></td>
						<td>
							<div id="email_err" class="inline-block none">
							
							</div>
							<div id="email_text" class="inline-block">
								<?php
									if (!$user_info['email']){
										echo '-';
									}else{
										echo $user_info['email'];
									}
								?>
							</div>
							<div id="email_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="email" name="email" id="email" class="form-control" value="<?=$user_info['email']?>" required>
									<button type="submit" name="povrd_email" id="profil_submit_email" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="nastav_val('email', $('#email_text').text().trim()); schovaj('email_err'); schovaj('email_form'); ukaz('email_edit'); ukaz('email_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="email_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť email" onclick="ukaz('email_form'); schovaj('email_text'); schovaj('email_edit');"></span>
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Dátum narodenia</strong></td>
						<td>
							<div id="datum_nar_err" class="inline-block none">
							
							</div>
							<div id="datum_nar_text" class="inline-block">
								<?php
									if ($user_info['datum_nar'] != '0000-00-00'){
										$pom_datum = date('d.m.Y',  strtotime($user_info['datum_nar']));
									}else{
										$pom_datum = '-';
									}
									echo $pom_datum;
								?>
							</div>
							<div id="datum_nar_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="date" name="datum_nar" id="datum_nar" class="form-control" value="<?=(($pom_datum != '-') ? $user_info['datum_nar'] : '0000-00-00')?>">
									<button type="submit" name="povrd_natum_nar" id="profil_submit_datum" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="nastav_val('datum_nar', $('#datum_nar_text').text().trim()); schovaj('datum_nar_err'); schovaj('datum_nar_form'); ukaz('datum_nar_edit'); ukaz('datum_nar_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="datum_nar_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť dátum narodenia" onclick="ukaz('datum_nar_form'); schovaj('datum_nar_text'); schovaj('datum_nar_edit');"></span>
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Výška</strong></td>
						<td>
							<div id="vyska_err" class="inline-block none">
							
							</div>
							<div id="vyska_text" class="inline-block">
								<?php
									if ($user_info['vyska'] == 0){
										echo '- cm';
									}else{
										echo $user_info['vyska'].' cm';
									}
								?>
							</div>
							<div id="vyska_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="number" min="0" name="vyska" id="vyska" class="form-control" value="<?=$user_info['vyska']?>">
									<button type="submit" name="povrd_vysku" id="profil_submit_vyska" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="schovaj('vyska_err'); schovaj('vyska_form'); ukaz('vyska_edit'); ukaz('vyska_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="vyska_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť výšku" onclick="ukaz('vyska_form'); schovaj('vyska_text'); schovaj('vyska_edit');"></span>
								</a>
							</div>
						</td>
					</tr>
					<tr>
						<td><strong>Váha</strong></td>
						<td>
							<div id="vaha_err" class="inline-block none">
							
							</div>
							<div id="vaha_text" class="inline-block">
								<?
									if ($user_info['vaha'] == 0){
										echo '- kg';
									}else{
										echo $user_info['vaha'].' kg';
									}
								?>
							</div>
							<div id="vaha_form" class="none sirka-100p">
								<form method="post" class="form-inline">
									<input type="number" min="0" name="vaha" id="vaha" class="form-control" value="<?=$user_info['vaha']?>">
									<button type="submit" name="povrd_vahu" id="profil_submit_vaha" class="btn btn-success">
										Uložiť
										<span class="glyphicon glyphicon-ok"></span>
									</button>
									<button type="button" class="btn btn-danger pull-right" onclick="schovaj('vaha_err'); schovaj('vaha_form'); ukaz('vaha_edit'); ukaz('vaha_text');">
										Zrušiť
										<span class="glyphicon glyphicon-remove"></span>
									</button>
								</form>
							</div>
							<div id="vaha_edit" class="<?=(($moznost_uprav) ? 'inline-block' : 'none')?>">
								<a href="javascript:void(0);">
									<span class="glyphicon glyphicon-pencil" title="Zmeniť váhu" onclick="ukaz('vaha_form'); schovaj('vaha_text'); schovaj('vaha_edit');"></span>
								</a>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<?php
				if ($moznost_uprav){
					?>
						<table class="table profil-tabulka table_supa_styl">
							<thead>
								<tr>
									<th>Administrácia</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div id="zmena_hesla_err">
										
										</div>
										<div id="zmena_hesla_button">
											<button class="btn btn-success" onclick="vymaz_val('pass1');vymaz_val('pass2');vymaz_obsah('zmena_hesla_text');ukaz('zmena_hesla_form'); schovaj('zmena_hesla_button');">
												Zmeniť heslo
											</button>
										</div>
										<div class="none" id="zmena_hesla_form">
											<form method="post" class="form-inline">
												<input type="password" id="pass1" name="pass1" placeholder="Heslo" class="form-control">
												<input type="password" id="pass2" name="pass2" placeholder="Heslo znovu" class="form-control">
												<button type="submit" class="btn btn-success" id="profil_submit_heslo">
													Zmeniť
													<span class="glyphicon glyphicon-ok"></span>
												</button>
												<button type="button" class="btn btn-danger pull-right" onclick="vymaz_obsah('zmena_hesla_err'); schovaj('zmena_hesla_form'); ukaz('zmena_hesla_button')">
													Zrušiť
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</form>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					<?php
				}
			?>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive nopadding">
			<section class="margin-bottom-40">
				<div class="margin-bottom-40">
					<h3>Udalosti</h3>
					<table class="table tablesorter table-hover table_supa_styl" id="myTable">
						<thead>
							<tr>
								<th>
									Názov
									<span class="glyphicon glyphicon-sort"></span>
								</th>
								<th class="xs-table-none">
									Miesto
									<span class="glyphicon glyphicon-sort"></span>
								</th>
								<th class="xs-table-none">
									Dátum
									<span class="glyphicon glyphicon-sort"></span>
								</th>
								<th class="xs-table-none">
									Čas
									<span class="glyphicon glyphicon-sort"></span>
								</th>
								<th class="xs-table-none">
									Účasť
									<span class="glyphicon glyphicon-sort"></span>
								</th>
								<th class="xs-table-zobraz">
									Dátum
									<span class="glyphicon glyphicon-sort"></span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if (count($udalosti) > 0){
									for ($i = 0; $i < count($udalosti); $i++){
										?>
											<tr>
												<td>
													<?=(($udalosti[$i]['typ'] == 2) ? '<span class="glyphicon glyphicon-star" title="Zápas"></span>' : '<span class="glyphicon glyphicon-record" title="Tréning"></span>')?>
													<a href="/udalosti/<?=$udalosti[$i]['id']?>"><?=$udalosti[$i]['nazov']?></a>
												</td>
												<td class="xs-table-none"><?=$udalosti[$i]['miesto']?></td>
												<td class="xs-table-none"><?=date('d.m.Y', strtotime($udalosti[$i]['datum']))?></td>
												<td class="xs-table-none"><?=date('H:i', strtotime($udalosti[$i]['cas']))?></td>
												<td class="xs-table-none"><?=$udalosti[$i]['pocet_ucastnikov']?></td>
												<td class="xs-table-zobraz">
													<?php
														echo date('d.m.Y', strtotime($udalosti[$i]['datum'])).' '.date('H:i', strtotime($udalosti[$i]['cas']));
													?>
												</td>
											</tr>
										<?php
									}
								}else{
									?>
										<tr>
											<td colspan="5">Žiadne nájdené udalosti</td>
										</tr>
									<?php
								}
							?>
						</tbody>
					</table>
					<?php
						if ($povolenie_vytvorit_tim){
							?>
								<button id="cudlik_pre_zalozenie_zapasu" type="button" class="btn btn-primary sirka-170 <?=((!$zobraz_form_vytvorenie_zapasu) ? 'display-block' : 'none')?>" onclick="ukaz('form_pre_zalozenie_zapasu');schovaj('cudlik_pre_zalozenie_zapasu');">Vytvoriť udalosť
								<span class="glyphicon glyphicon-calendar"></span>
								</button>
							<?php
						}
					?>
				</div>
				<div class="margin-bottom-40">
					<?php
						if ($povolenie_vytvorit_tim){
							?>
								<div id="form_pre_zalozenie_zapasu" class="nopadding table-responsive <?=(($zobraz_form_vytvorenie_zapasu) ? 'display-block' : 'none')?>">
									<?php
										$form['nazov'] = array(
											'name' 			=> 'nazov',
											'value'			=> $this->input->post('nazov'),
											'class' 		=> 'form-control',
											'placeholder'	=> '(napr. Zakončenie sezóny)',
											'required'		=> 'required'
										);
										$form['miesto'] = array(
											'name' 			=> 'miesto',
											'value'			=> $this->input->post('miesto'),
											'class' 		=> 'form-control',
											'placeholder'	=> '(napr. Ihrisko v Pezinku)',
											'required'		=> 'required'
										);
										$form['popis'] = array(
											'name' 			=> 'popis',
											'value'			=> $this->input->post('popis'),
											'class' 		=> 'form-control noresize',
											'placeholder'	=> 'Popis...'
										);

										echo form_open('profil/'.$this->session->userdata('id').'/vytvor_zapas');
											?>
												<table class="table table-borderless xs-table-block">
													<tr>
														<td><strong>Cieľová skupina</strong></td>
														<td>
															<select id="verejne" name="verejne" class="form-control">
																<option value="0">Verejná udalosť</option>
																<?php
																	for ($i = 0; $i < count($moje_timy); $i++){
																		?>
																			<option value="<?=$moje_timy[$i]['id']?>" <?=($this->input->post('verejne') == $moje_timy[$i]['id'] ? 'selected' : '')?>>
																				Tím - <?=$moje_timy[$i]['nazov']?>
																			</option>
																		<?php
																	}
																?>
															</select>
														</td>
													</tr>
													<tr>
														<td class=""><strong>Typ udalosti</strong></td>
														<td>
															<select id="typ_zapasu" name="typ_zapasu" class="form-control">
																<option value="1" <?=($this->input->post('typ_zapasu') == 1 ? 'selected' : '')?>>Tréning</option>
																<option value="2" <?=($this->input->post('typ_zapasu') == 2 ? 'selected' : '')?>>Zápas</option>
															</select>
														</td>
													</tr>
													<tr>
														<td><strong>Názov udalosti</strong></td>
														<td><?=form_input($form['nazov']);?></td>
													</tr>
													<tr>
														<td class="nopadding-top-bot"></td>
														<td class="nopadding-top-bot"><?=form_error('nazov');?></td>
													</tr>
													<tr>
														<td><strong>Miesto konania</strong></td>
														<td><?=form_input($form['miesto']);?></td>
													</tr>
													<tr>
														<td class="nopadding-top-bot"></td>
														<td class="nopadding-top-bot"><?=form_error('miesto');?></td>
													</tr>
													<tr>
														<td><strong>Dátum</strong></td>
														<td><input type="date" name="datum_konania" id="datum_konania" class="form-control" placeholder="dd.mm.rrrr" value="<?=$this->input->post('datum_konania')?>" required></td>
													</tr>
													<tr>
														<td class="nopadding-top-bot"></td>
														<td class="nopadding-top-bot"><?=form_error('datum_konania');?></td>
													</tr>
													<tr>
														<td><strong>Čas</strong></td>
														<td><input type="time" name="cas_udalosti" class="form-control" placeholder="hh:mm" value="<?=$this->input->post('cas_udalosti')?>" required></td>
														
													</tr>
													<tr>
														<td class="nopadding-top-bot"></td>
														<td class="nopadding-top-bot"><?=form_error('cas_udalosti');?></td>
													</tr>
													<tr>
														<td colspan="2"><?=form_textarea($form['popis']);?></td>
													</tr>
													<tr>
														<td class="padding-top-25" colspan="2">
															<button type="submit" class="btn btn-success sirka-170" name="submit">
																Vytvoriť udalosť
																<span class="glyphicon glyphicon-ok"></span>
															</button>
															<button type="button" class="btn btn-danger pull-right" onclick="schovaj('form_pre_zalozenie_zapasu');ukaz('cudlik_pre_zalozenie_zapasu');">
																Zrušiť
																<span class="glyphicon glyphicon-remove"></span>
															</button>
														</td>
													</tr>
												</table>
											<?php
										echo form_close();
									?>
								</div>
							<?php
						}
					?>
				</div>
			</section>
			<section>
				<h3>Členstvo v tímoch</h3>
				<table class="table tablesorter table-hover table_supa_styl" id="myTable2">
					<thead>
						<tr>
							<th>
								Názov tímu
								<span class="glyphicon glyphicon-sort"></span>
							</th>
							<th>
								Počet členov
								<span class="glyphicon glyphicon-sort"></span>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if (count($timy) > 0){
								for ($i = 0; $i < count($timy); $i++){
									?>
										<tr>
											<td><a href="/timy/<?=$timy[$i]['id']?>"><?=$timy[$i]['nazov']?></a></td>
											<td><?=$timy[$i]['pocet_clenov']?></td>
										</tr>
									<?php
								}
							}else{
								?>
									<tr>
										<td colspan="2">Žiadne nájdené tímy</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<?php
					if ($povolenie_vytvorit_tim){
						?>
							<button id="button_zaloz_tim" type="button" class="btn btn-primary sirka-170 <?=((!$zobraz_form_vytvorenie_timu) ? 'display-block' : 'none')?>" onclick="ukaz_form_pre_zalozenie_timu();">
								Založiť nový tím
								<span class="glyphicon glyphicon-plus"></span>
							</button>
						<?php
					}
				?>
			</section>
		</div>
		<?php
			if ($povolenie_vytvorit_tim){
				?>
					<div id="form_pre_zalozenie_timu" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive nopadding <?=(($zobraz_form_vytvorenie_timu) ? 'display-block' : 'none')?>">
						<?php
							$form['nazov'] = array(
								'name' 			=> 'nazov',
								'value'			=> $this->input->post('nazov'),
								'class' 		=> 'form-control',
								'placeholder'	=> 'Názov tímu',
								'required'		=> 'required'
							);
							$form['popis'] = array(
								'name' 			=> 'popis',
								'value'			=> $this->input->post('popis'),
								'class' 		=> 'form-control noresize',
								'placeholder'	=> 'Popis...'
							);

							echo form_open('profil/'.$this->session->userdata('id').'/vytvor_team');
								?>
									<table class="table table-borderless xs-table-block">
										<tr>
											<td><strong>Názov tímu</strong></td>
											<td><?=form_input($form['nazov']);?></td>
										</tr>
										<tr>
											<td colspan="2" class="nopadding"><?=form_error('nazov');?></td>
										</tr>
										<tr>
											<td colspan="2"><?=form_textarea($form['popis']);?></td>
										</tr>
										<tr>
											<td class="padding-top-25" colspan="2">
												<button type="submit" class="btn btn-success sirka-170" name="submit">
													Založiť tím
													<span class="glyphicon glyphicon-ok"></span>
												</button>
												<button type="button" class="btn btn-danger pull-right" onclick="skry_form_pre_zalozenie_timu();">
													Zrušiť
													<span class="glyphicon glyphicon-remove"></span>
												</button>
											</td>
										</tr>
									</table>
								<?php
							echo form_close();
						?>
					</div>
				<?php
			}
		?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
			<?php
				if (($this->uri->segment(2) != $this->session->userdata('id')) && ($this->session->userdata('admin') == 1)){
					?>
						<div id="cudlik_ban_usera">
							<a href="javascript:void(0);" onclick="ukaz('form_ban_usera'); schovaj('cudlik_ban_usera');" class="btn btn-danger sirka-200">
								Zablokovať užívateľa <span class="glyphicon glyphicon-ban-circle"></span>
							</a>
						</div>
						<div id="form_ban_usera" class="none">
							<form action="<?= base_url()?>profil/<?=$user_info['id']?>/ban_usera" method="post" accept-charset="utf-8" class="form-inline">
								<label for="potvrdzovacie_heslo">Zadajte vaše heslo: </label>
								<input type="password" class="form-control" name="potvrdzovacie_heslo" id="potvrdzovacie_heslo">
								<button type="submit" class="btn btn-danger">
									Potvrdiť <span class="glyphicon glyphicon-ban-circle"></span>
								</button>
								<button type="button" class="btn btn-danger pull-right" onclick="ukaz('cudlik_ban_usera'); schovaj('form_ban_usera');">
									Zrušiť <span class="glyphicon glyphicon-remove-circle"></span>
								</button>
							</form>
						</div>

					<?php
				}
			?>
		</div>
	</div>

<?php $this->load->view('footer'); ?>