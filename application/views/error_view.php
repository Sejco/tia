<?php $this->load->view('header'); ?>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 container_header">	
		<h1>Ooops!</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 container">	
		<p><?=$error_text?></p>
	</div>
<?php $this->load->view('footer'); ?>