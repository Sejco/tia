<?php $this->load->view('header'); ?>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container_header">
		<h1>Prihlásenie</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container">
		<?php
			if($this->session->flashdata('message')){
				?>
					<div id="flash-messages" class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('message');?>
					</div>
				<?php
			}
		?>	
		<div class="sirka-200 center">
			
			<?php
				$inputLogin = array(
					'name' 			=> 'email',
					'value'			=> $this->input->post('email'),
					'class' 		=> 'form-control',
					'placeholder'	=> 'Email',
					'type'			=> 'email',
					'required'      => 'required'
				);
				$inputPass	= array(
					'name' 			=> 'pass',
					'class' 		=> 'form-control',
					'placeholder'	=> 'Heslo',
					'required'      => 'required'
				);
				$inputSubmit = array(
					'name' 			=> 'submit',
					'value'			=> 'Prihlásiť',
					'class'			=> 'btn btn-primary margin-bottom-5 sirka-100p'
				);

				echo form_open('login');
					?>
						<table class="table table-borderless center xs-table-block">
							<tr>
								<td class="nopadding-left-right"><?=form_input($inputLogin);?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_password($inputPass);?></td>
							</tr>
							<tr>
								<td class="nopadding-left-right">					
									<p><?=anchor('password', 'Zabudli ste heslo?');?><br /><?=anchor('registracia', 'Registrácia');?></p>
								</td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_submit($inputSubmit);?></td>
							</tr>	
								
						</table>
					<?php	
				echo form_close();
			?>
			<div class="center sirka-170 err_msg <?=$zobraz_err_div?>">
				<p class="text-center">Nesprávne údaje!</p>
			</div>
		</div>
	</div>

<?php $this->load->view('footer'); ?>