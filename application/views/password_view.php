<?php $this->load->view('header'); ?>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container_header">
		<h1>Obnova hesla</h1>
	</div>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-none container">
		<?php
			if($this->session->flashdata('message')){
				?>
					<div id="flash-messages" class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('message');?>
					</div>
				<?php
			}elseif($this->session->flashdata('error')){
				?>
					<div id="flash-messages" class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<?=$this->session->flashdata('error');?>
					</div>
				<?php
			}
		?>
		<div class="sirka-200 center">

			<?php
				$inputLogin = array(
					'name' 			=> 'email',
					'value'			=> $this->input->post('email'),
					'class' 		=> 'form-control',
					'placeholder'	=> 'Email',
					'type'			=> 'email',
					'required'      => 'required'
				);
				$inputSubmit = array(
					'name' 			=> 'submit',
					'value'			=> 'Obnoviť heslo',
					'class'			=> 'btn btn-primary margin-bottom-5 sirka-100p'
				);

				echo form_open('password');
					?>
						<table class="table table-borderless center xs-table-block">
							<tr>
								<td class="nopadding-left-right">
									<strong>Zadajte Váš email:</strong>
								</td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_input($inputLogin);?></td>
							</tr>
							<tr>
								<td class="nopadding">
									<p class="pull-right"><?=anchor('login', 'Spať na prihlásenie');?></p>
								</td>
							</tr>
							<tr>
								<td class="nopadding"> 
									<p class="pull-right"><?=anchor('registracia', 'Registrácia');?></p>
								</td>
							</tr>
							<tr>
								<td class="nopadding-left-right"><?=form_submit($inputSubmit);?></td>
							</tr>

						</table>
					<?php
				echo form_close();
			?>
		</div>
	</div>

<?php $this->load->view('footer'); ?>