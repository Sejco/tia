<?php $this->load->view('header'); ?>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container_header">
		<h1>Tímy</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container">	
		<div class="table-responsive">		
			<form action="<?= base_url()?>timy/hladat/" class="form-inline margin-bottom-15 pull-right" method="get" accept-charset="utf-8">
				<div class="input-group">
					<input type="text" name="timy_search" id="timy_search" placeholder="Zadajte výraz" class="form-control" value="<?=$this->input->get('timy_search')?>">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-search"></span> Vyhľadať
						</button>
					</div>
				 </div>
			</form>		
			<table class="table tablesorter table-hover table_supa_styl" id="myTable">
				<thead>
					<tr>
						<th>
							Názov tímu
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th>
							Počet členov
							<span class="glyphicon glyphicon-sort"></span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (count($timy) > 0){
							for ($i = 0; $i < count($timy); $i++){
								?>
									<tr>
										<td><a href="/timy/<?=$timy[$i]['id']?>"><?=$timy[$i]['nazov']?></a></td>
										<td><?=$timy[$i]['pocet_clenov']?></td>									
									</tr>
								<?php
							}
						}else{
							?>
								<tr>
									<td  colspan="2"><p>Nenašli sa žiadne výsledky.</p></td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

<?php $this->load->view('footer'); ?>
