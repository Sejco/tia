<?php $this->load->view('header'); ?>

	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container_header">
		<h1>Udalosti</h1>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2 xs-padding-sides-5 container">
		<div class="table-responsive">
			<form action="<?= base_url()?>udalosti/hladat/" class="form-inline margin-bottom-15 pull-right" method="get" accept-charset="utf-8">
				<div class="input-group">
					<input type="text" name="udalosti_search" id="udalosti_search" placeholder="Zadajte výraz" class="form-control" value="<?=$this->input->get('udalosti_search')?>">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-primary">
							<span class="glyphicon glyphicon-search"></span> Vyhľadať
						</button>
					</div>
				 </div>
			</form>		
			<table class="table tablesorter table-hover table_supa_styl" id="myTable">
				<thead>
					<tr>
						<th>
							Názov
							<span class="glyphicon glyphicon-sort"></span>
						</th>					
						<th class="xs-table-none">
							Miesto konania
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Dátum
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Čas
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-none">
							Účasť
							<span class="glyphicon glyphicon-sort"></span>
						</th>
						<th class="xs-table-zobraz">
							Dátum
							<span class="glyphicon glyphicon-sort"></span>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (count($udalosti) > 0){
							for ($i = 0; $i < count($udalosti); $i++){
								?>
									<tr>
										<td>
											<?=(($udalosti[$i]['typ'] == 2) ? '<span class="glyphicon glyphicon-star" title="Zápas"></span>' : '<span class="glyphicon glyphicon-record" title="Tréning"></span>')?>
											<a href="/udalosti/<?=$udalosti[$i]['id']?>"><?=$udalosti[$i]['nazov']?></a>
										</td>
										<td class="xs-table-none">
											<?=$udalosti[$i]['miesto']?>
										</td>
										<td class="xs-table-none">
											<?=date('d.m.Y', strtotime($udalosti[$i]['datum']))?>
										</td>
										<td class="xs-table-none">
											<?=date('H:i', strtotime($udalosti[$i]['cas']))?>
										</td>
										<td class="xs-table-none">
											<?=$udalosti[$i]['pocet_ucastnikov']?>
										</td>
										<td class="xs-table-zobraz">
											<?php
												echo date('d.m.Y', strtotime($udalosti[$i]['datum'])).' '.date('H:i', strtotime($udalosti[$i]['cas']));
											?>
										</td>
									</tr>
								<?php
							}
						}else{
							?>
								<tr>
									<td  colspan="2"><p>Nenašli sa žiadne výsledky.</p></td>
								</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>

<?php $this->load->view('footer'); ?>