<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap-theme.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/style.css">


	<script src="<?= base_url()?>assets/js/jquery-1.11.2.min.js"></script>
	<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>	
	<script src="<?= base_url()?>assets/js/jquery-latest.js"></script>
	<script src="<?= base_url()?>assets/js/jquery.tablesorter.min.js"></script>
	
	<script src="<?= base_url()?>assets/js/main.js"></script>
	
	<script type="text/javascript">
		base_url = '<?=base_url()?>';
	</script>

	<title><?=$title?></title>
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<header>
			<div class="container-fluid xs-padding-sides-none">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 nopadding">
					<div class="navigacia padding-top-10">
						<nav>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 nopadding">
								<ul>
									<li>
										<a href="/home/" title="Domov" class="<?=(($this->uri->segment(1) == 'home') ? 'nav_active_bg' : '')?>">
											<span class="xs-none">Domov</span>
											<span class="glyphicon glyphicon-home xs-block" aria-hidden="true"></span>
										</a>
									</li>
									<li>
										<a href="/udalosti/" title="Udalosti" class="<?=(($this->uri->segment(1) == 'udalosti') ? 'nav_active_bg' : '')?>">
											<span class="xs-none">Udalosti</span>
											<span class="glyphicon glyphicon-calendar xs-block"></span>
										</a>
									</li>
									<li>
										<a href="/timy/" title="Tímy" class="<?=(($this->uri->segment(1) == 'timy') ? 'nav_active_bg' : '')?>">
											<span class="xs-none">Tímy</span>
											<span class="glyphicon glyphicon-star xs-block"></span>
										</a>
									</li>
									<li>
										<a href="/hraci/" title="Hráči" class="<?=(($this->uri->segment(1) == 'hraci') ? 'nav_active_bg' : '')?>">
											<span class="xs-none">Hráči</span>
											<span class="glyphicon glyphicon-knight xs-block"></span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 nopadding">
								<?php
									if($this->session->userdata('prihlaseny')){
										?>
											<table class="pull-right">
												<tr>
													<td>
														<a href="/profil/<?=$this->session->userdata('id')?>" title="Profil používateľa" class="<?=(($this->uri->segment(1) == 'profil') ? 'nav_active_bg' : '')?>">
															<span class="glyphicon glyphicon-user xs-block" aria-hidden="true"></span>
															<span class="xs-none"><?=$this->session->userdata('meno')?></span>												
														</a>
													</td>
													<td>
														<a href="/logout/" title="Odhlásiť">
															<span class="glyphicon glyphicon-log-out xs-block" aria-hidden="true"></span>
															<span class="xs-none">Odhlásiť</span>	
														</a>
													</td>
												</tr>												
											</table>
										<?php
									}else{
										?>											
											<a href="/login/" title="Login" class="pull-right <?=(($this->uri->segment(1) == 'login') ? 'nav_active_bg' : '')?>">
												<span class="glyphicon glyphicon-log-in xs-block" aria-hidden="true"></span>
												<span class="xs-none">Login</span>	
											</a>							
										<?php
									}
								?>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
	</div>
	<div class="container-fluid">
		<div class="row" id="main">
	